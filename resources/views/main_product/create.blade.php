@extends('layouts.shop')
@extends('layouts.header2')
@section('content')
<!-- <div class="navbar-brand" role="group" aria-label="Basic example">
   <a href="{{ url('/mainproducts') }}"><button type="button" style="width: 100" class="btn btn-secondary">Product List</button></a>
</div> -->
@if($products_data=='create')
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Create Main-Product')?></h1>
</div>
{!! Form::open(['action' => 'MainProductsController@store','method'=>'Post','enctype'=>'multipart/form-data']) !!}
<fieldset class="form-group">
   {{ Form::label('content', ('Product Name')) }}
   {{ Form::textarea('product_name', $value = null ,  $attributes = array('class'=>'form-control ', 'rows'=>'1', 'ng-model'=>'content' ,'placeholder' => 'Enter Product Name')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Description')) }}
   {{ Form::textarea('product_description', $value = null ,  $attributes = array('class'=>'form-control ', 'rows'=>'5', 'ng-model'=>'content' ,'placeholder' => 'Enter Description')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Image')) }}
   <input type="file" name="product_image" id="product_image">
   <img src="" id="profile-img-tag" class="mt-4" width="100px" height="60px">
</fieldset>
<div class="buttons text-center">
   {{Form::submit('ADD',['class' => 'btn btn-lg btn-success button'])}}
</div>
{!! Form::close() !!}
@else
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Edit Main Products')?></h1>
</div>
{!! Form::Open(['action'=>['MainProductsController@update',$product->id],'method'=>'Post','enctype'=>'multipart/form-data']) !!}
{{method_field('PATCH')}}
<fieldset class="form-group">
   {{ Form::label('content', ('product_name')) }}
   {{ Form::textarea('product_name', $value = $product->product_name ,  $attributes = array('class'=>'form-control ', 'rows'=>'5', 'ng-model'=>'content' ,'placeholder' => 'enter product name')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('description')) }}
   {{ Form::textarea('product_description', $product->product_description ,  $attributes = array('class'=>'form-control ', 'rows'=>'5', 'ng-model'=>'content' ,'placeholder' => 'enter description')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Image')) }}
   <input type="file" name="product_image" id="product_image">
   <img src="<?php echo asset("/public/photo/{$product->product_image}")?>" id="profile-img-tag" class="mt-4" width="100px" height="60px">
</fieldset>
<div class="buttons text-center">
   {{Form::submit('Update',['class' => 'btn btn-lg btn-success button'])}}
</div>
{!! Form::close() !!}
@endif
@endsection