@extends('layouts.app')
@section('content')
@include('layouts.header2')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-11">
         <div class="navbar-brand" role="group" aria-label="Basic example">
            <a  href="{{ url('/mainproducts/create') }}"><button type="button" style="width: 100" class="btn btn-secondary">New</button></a>
         </div>
         <div class="card-body" style="background-color: #eee; width: 1140px;height: 490px; border: 2px solid black; overflow: auto;" id="">
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status') }}
            </div>
            @endif
            @if(count($products)>0)
            <div class="row">
               <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Main Products List</th>
                        <th> </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($products as $product)
                     <tr>
                        <td class="col-sm-8 col-md-6" style="text-align: center">
                           <div class="media">
                              <div class="photo">
                                 <a class="thumbnail pull-left" href="#" style=""> <img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"> </a>
                              </div>
                              <!-- END OF PHOTO -->
                              <div class="media-body">
                                 <h4  class="product-title" ><a  href="/products/{{$product->id}} ">{{$product->product_name}}</a></h4>
                                 <span>Description :  </span><span class="text-success"><strong> {{str_limit($product->product_description, 25)}};</strong></span><br>
                                 <!-- <span>Created at :  </span><span class="text-success"><strong>{{$product->created_at}}</strong></span> -->
                              </div>
                           </div>
                        </td>
                        <td class="col-sm-1" style="text-align: center">
                           

                            <button type="button" class="btn btn-danger deletebutton" value="{{$product->id}}" >
                           Delete
                           </button>
                           <a href="{{url('main_products', $product->id) }}" class="btn btn-primary">EDIT</a>
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong1/{{$product->id}}">
                           view
                           </button>
                                
                        </td>
                     </tr>
                     <div class="modal fade" id="exampleModalLong1/{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h5 class="modal-title" id="exampleModalLongTitle">Main product Details</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="modal-body" >
                                 <div class="preview-pic tab-content">
                                    <div class="tab-pane active" id="pic-1"><img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"></div>
                                 </div>
                                 <h3 class="product-title">{{$product->product_name}}</h3>
                                 <div class="rating">
                                    <div class="stars">
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star"></span>
                                       <span class="fa fa-star"></span>
                                    </div>
                                 </div>
                                 <p class="product-description" style="word-wrap: break-word;">{{$product->product_description}}</p>
                                 ...
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </tbody>
               </table>
            </div>
            {!! $products->render() !!}
            @else
            <h1 class="display-4">THERE'S NO PRODUCT<BR><a href="{{ url('/mainproducts/create') }}">ADD PRODUCT </a>  </h1>
            @endif
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $('.deletebutton').on('click', function () {
   var id=$(this).val();
   //alert(id);

        // return confirm('Are you sure want to delete?');
        event.preventDefault();//this will hold the url
        swal({
            title: "Are you sure?",
            text: "Once clicked, this will be softdeleted!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              $.ajax({url: "main_products_delete/"+id, success: function(result){
                swal("Done! product has been deleted!", {
                    icon: "success",
                    button: false,
                });
            location.reload(true);
                    $('#subproduct_id').html(result);
                }})
               //this will release the event
            } else {
               alert('failed');
                swal("Your imaginary file is safe!");
            }
        });
    });
</script>
         </div>
      </div>
   </div>
</div>

@endsection