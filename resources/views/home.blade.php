@extends('layouts.app')
@section('content')
@include('layouts.header2')
<div class="container">
   <div class="row justify-content-left col-md-10">
      <div class="col-md-8">
         <div class="navbar-brand" role="group" aria-label="Basic example">
            <div class="Row">
               <div class="Column">
                  <a href="{{ url('/products/create') }}">
                     <button type="button" class="btn btn-secondary">New</button>
                  </a>
               </div>
               <div class="Column" style="width:200px;">
                  <select class="form-control getsubproduct_id_by_mainproduct" name="mainproduct_id">
                     <option value="all">Select Main Product</option>
                     @if(count($main_product))
                     @foreach($main_product as $main_product)
                     <option value="{{$main_product->id}}">{{$main_product->product_name}}</option>
                     @endforeach
                     @else
                     no data found
                     @endif
                  </select>
               </div>

               <div class="Column" style="width:200px;">
                  <select class="form-control getproduct_by_subproduct" name="subproduct_id" id="subproduct_id">
                     <option value="all">Select Sub Product</option>
                  </select>    
               </div>
            </div>
            
         </div>
         <div class="card-body" style="background-color: #eee;width: 1170px;height: 490px;border: 2px solid black;overflow: auto;">
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status') }}
            </div>
            @endif
            <div id="product_id_show">
               @if(count($products)>1)
               <div class="row" >
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>Product List</th>
                           <th class="text-center"></th>
                           <th> </th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($products as $product)
                        <tr>
                           <td class="col-sm-8 col-md-6" style="text-align: center">
                              <div class="media">
                                 <div class="photo">
                                    <a class="thumbnail pull-left" href="products/{{$product->id}}" style=""> <img src = "<?php echo asset("public/photo/{$product->product_image}")?>"> </a>
                                 </div>
                                 <!-- END OF PHOTO -->
                                 <?php
                                    // $mainproduct=App\Main_Products::where('id',$product->main_products_id)->first();
                                    // if (!empty($mainproduct)) {
                                    //    $mainproduct_name=$mainproduct->product_name;
                                    // }
                                    // else
                                    // {
                                    //    $mainproduct_name='--';
                                    // }
                                    // $subproduct=App\Sub_Products::where('id',$product->sub_products_id)->first();
                                    // if (!empty($subproduct)) {
                                    //    $subproduct_name=$subproduct->product_name;
                                    // }
                                    // else
                                    // {
                                    //    $subproduct_name='--';
                                    // }
                                    ?>
                                 <div class="media-body">
                                    <a  href="/products/{{$product->id}} ">{{$product->product_name}}</a>
                                    <!-- <span>Main Product :  </span><span class="text-success"><strong></strong></span><br>
                                       <span>Sub Product :  </span><span class="text-success"><strong></strong></span><br> -->
                                    <!-- <span>Created at :  </span><span class="text-success"><strong>{{$product->created_at}}</strong></span> -->
                                 </div>
                                 <div class="media-body">
                                    <strong>  {{$product->amount}}.00</strong>
                                 </div>
                                 <div class="media-body">
                                    <a  href="/products/{{$product->id}} ">{{$product->stock_manage}}</a>
                                 </div>
                              </div>
                           </td>
                           <td class="col-sm-1 col-md-1" style="text-align: center">
                              <button type="button" class="btn btn-danger deletebutton" value="{{$product->id}}" >
                              Delete
                              </button>
                              <a href="{{url('products_edit', $product->id) }}" class="btn btn-primary">EDIT</a>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                  &nbsp;&nbsp;&nbsp;
                  {!! $products->render() !!}
                  @else
                  <h1 class="display-4">Please select main product and Sub product  </h1>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset('public/js/ajaxcall.js') }}"></script>
@endsection