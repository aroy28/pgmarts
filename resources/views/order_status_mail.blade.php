<!DOCTYPE html>
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
   <head>
      <title></title>
      <meta charset="utf-8"/>
      <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
      <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
      <style>
         * {
         box-sizing: border-box;
         }
         body {
         margin: 0;
         padding: 0;
         }
         a[x-apple-data-detectors] {
         color: inherit !important;
         text-decoration: inherit !important;
         }
         #MessageViewBody a {
         color: inherit;
         text-decoration: none;
         }
         p {
         line-height: inherit
         }
         @media (max-width:700px) {
         .fullMobileWidth,
         .row-content {
         width: 100% !important;
         }
         .image_block img.big {
         width: auto !important;
         }
         .mobile_hide {
         display: none;
         }
         .stack .column {
         width: 100%;
         display: block;
         }
         .mobile_hide {
         min-height: 0;
         max-height: 0;
         max-width: 0;
         overflow: hidden;
         font-size: 0px;
         }
         }
      </style>
   </head>
   <body style="margin: 0; background-color: #ffffff; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
      <table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff;" width="100%">
         <tbody>
            <tr>
               <td>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #9fd1ae;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                             <tr>
                                                <td style="width:100%;padding-right:0px;padding-left:0px;padding-top:45px;">
                                                   <div align="center" style="line-height:10px"><a href="https://www.pgmarts.com" style="outline:none" tabindex="-1" target="_blank"><img src="https://www.pgmarts.com/public/images/logo.png" alt="PGMARTS" src="" style="display: block; height: auto; border: 0; width: 204px; max-width: 100%;" title="PG MART" width="204"/></a></div>
                                                </td>
                                             </tr>
                                          </table>
                    
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:20px;padding-left:60px;padding-right:60px;padding-top:30px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 46.800000000000004px;"><span style="font-size:26px;">Your Order Has Been Successfully Placed.</span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
                                          <div class="spacer_block" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:5px;padding-left:30px;padding-right:30px;padding-top:15px;">
                                                   <div style="font-family: sans-serif">
                                                   	<div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left;"><strong><span style="font-size:16px;">Order Status :<?php if(!empty($orders_data)){ echo $orders_data['order_status']; } ?></span></strong></p>
                                                      </div>

                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left;"><strong><span style="font-size:16px;">Where It's Going:</span></strong></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:15px;padding-left:30px;padding-right:30px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 28.8px;"><span style="font-size:16px;"><strong><span style=""><?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['name']; } ?></span></strong></span></p>
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 28.8px;"><span style="font-size:16px;"><span style=""><?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['house_no']; } ?> <?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['address_info']; } ?></span></span></p>
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 28.8px;"><span style="font-size:16px;"><span style=""><?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['pincode']; } ?>, <?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['area_locality']; } ?></span></span></p>
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 28.8px;"><span style="font-size:16px;background-color:transparent;"><?php if(isset($orders_data['ordered_address']) && !empty($orders_data['ordered_address'])){ echo $orders_data['ordered_address']['phone']; } ?></span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:5px;padding-left:30px;padding-right:30px;padding-top:15px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left;"><strong><span style="font-size:16px;">Payment:</span></strong></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:30px;padding-right:30px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #d19fc2; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 46.800000000000004px;"><span style="font-size:26px;"><strong>₹ <?php if(!empty($orders_data) ){ echo $orders_data['grand_total']; } ?></strong></span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:15px;padding-left:30px;padding-right:30px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 28.8px;"><span style="font-size:16px;">Payment via <?php if(!empty($orders_data) ){ echo $orders_data['pay_via']; } ?></span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="divider_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:30px;padding-left:10px;padding-right:10px;padding-top:30px;">
                                                   <div align="center">
                                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                                                         <tr>
                                                            <td class="divider_inner" style="font-size: 1px; line-height: 1px; border-top: 1px solid #BBBBBB;"><span> </span></td>
                                                         </tr>
                                                      </table>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-5" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:5px;padding-left:30px;padding-right:30px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 21.6px; color: #33563c; line-height: 1.8;">
                                                         <p style="margin: 0; font-size: 14px; text-align: left;"><strong><span style="font-size:16px;">What Your Order Includes</span></strong></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>

                  <?php 
                      if(isset($orders_data['ordered_products']) && !empty($orders_data['ordered_products'])){ 
                        foreach($orders_data['ordered_products'] as  $product){ 

                   ?>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-6" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:10px;padding-left:30px;padding-right:10px;padding-top:40px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #232323; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><span style="font-size:17px;"><?php  echo $product['product_name'].' Qty: '. $product['quantity'];  ?> </span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:30px;padding-right:10px;padding-top:10px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 18px; color: #848484; line-height: 1.5; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px; mso-line-height-alt: 21px;"><span style="font-size:14px;"><?php  echo $product['product_description'];  ?> </span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:30px;line-height:30px;font-size:1px;"> </div>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:15px;padding-left:30px;padding-right:10px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;">₹ 46.99</p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                <?php 
                    }
                  }

                ?>
         <!--    order price details here -->
                  
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-11" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-left: 10px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:35px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #232323; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><span style="font-size:14px;">Delivery:</span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:30px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><?php if(!empty($orders_data['delivery_charge']) ){ echo $orders_data['delivery_charge']; } else { echo 'Free'; } ?></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-12" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-left: 10px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:35px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #232323; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><span style="font-size:14px;">Tax:</span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:30px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;">₹ <?php if(!empty($orders_data['tax']) ){ echo $orders_data['tax']; } else { echo '0.00'; } ?></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-13" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-left: 10px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:35px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #232323; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><span style="font-size:18px;">Order Total</span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-left:30px;padding-right:10px;padding-top:15px;padding-bottom:5px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #d19fc2; line-height: 1.2; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; font-size: 14px;"><span style="font-size:18px;">₹ <?php echo $orders_data['grand_total']; ?></span></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-14" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #ffffff; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-left: 10px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                      
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="25%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:25px;line-height:25px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                 
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-16" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #9fd1ae;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                          <div class="spacer_block mobile_hide" style="height:20px;line-height:20px;font-size:1px;"> </div>
                                          <div class="spacer_block" style="height:5px;line-height:5px;font-size:1px;"> </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                 
                  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-21" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #9fd1ae;" width="100%">
                     <tbody>
                        <tr>
                           <td>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 680px;" width="680">
                                 <tbody>
                                    <tr>
                                       <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="50%">
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:10px;padding-left:30px;padding-right:10px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #33563c; line-height: 1.2;">
                                                         <p style="margin: 0; font-size: 18px; text-align: left;"><strong><span style="">Where To Find Us</span></strong></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                          <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
                                             <tr>
                                                <td style="padding-bottom:10px;padding-left:30px;padding-right:10px;padding-top:10px;">
                                                   <div style="font-family: sans-serif">
                                                      <div style="font-size: 12px; mso-line-height-alt: 18px; color: #33563c; line-height: 1.5; font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;">
                                                         <p style="margin: 0; mso-line-height-alt: 18px;"><span style="font-size:12px;"><a href="http://www.example.com" rel="noopener" style="text-decoration: underline; color: #33563c;" target="_blank">https://www.pgmarts.com</a></span></p>
                                                         <p style="margin: 0; mso-line-height-alt: 18px;"></p>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  
               </td>
            </tr>
         </tbody>
      </table>
      <!-- End -->
   </body>
</html>