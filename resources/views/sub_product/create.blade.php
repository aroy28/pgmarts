@extends('layouts.shop')
@extends('layouts.header2')
@section('content')
<!-- <div class="navbar-brand" role="group" aria-label="Basic example">
   <a href="{{ url('/subproducts') }}"><button type="button" style="width: 100" class="btn btn-secondary">Product List</button></a>
</div> -->
@if($products_data=='create')
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Create Sub-Product')?></h1>
</div>
{!! Form::open(['action' => 'SubProductsController@store','method'=>'Post','enctype'=>'multipart/form-data']) !!}
<fieldset class="form-group">
   <label for="colFormLabel" class="col-sm-3 col-form-label">Main Product <span class="start_color">*</span></label>
   <div class="col-sm-14">
      <select required class="custom-select my-1 mr-sm-2 cst_select" id="main_products_id" name="main_products_id">
         <option selected value="">Choose...</option>
         @php $prod = App\Main_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endforeach
         @endif
      </select>
   </div>
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Product Name *')) }}
   {{ Form::textarea('product_name', $value = null ,  $attributes = array('class'=>'form-control ', 'rows'=>'1', 'ng-model'=>'product_name','id'=>'product_name' ,'placeholder' => 'enter product name')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Description')) }}
   {{ Form::textarea('product_description', $value = null ,  $attributes = array('class'=>'form-control ', 'rows'=>'3', 'ng-model'=>'product_description','id'=>'product_description' ,'placeholder' => 'enter description')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Image')) }}
   <input type="file" name="product_image" id="product_image">
   <img src="" id="profile-img-tag" class="mt-4" width="100px" height="60px">
</fieldset>
<div class="buttons text-center">
   {{Form::submit('ADD',['class' => 'btn btn-lg btn-success button senddata'])}}
</div>
{!! Form::close() !!}
@else
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Edit Sub Products')?></h1>
</div>
{!! Form::Open(['action'=>['SubProductsController@update',$product->id],'method'=>'Post','enctype'=>'multipart/form-data']) !!}
{{method_field('PATCH')}}
<fieldset class="form-group">
   <label for="colFormLabel" class="col-sm-3 col-form-label">Edit Main Product <span class="start_color">*</span></label>
   <div class="col-sm-14">
      <select class="custom-select my-1 mr-sm-2 cst_select" id="main_products_id" name="main_products_id">
         @php $prod = App\Main_Products::where('id',$product->main_products_id)->first(); @endphp
         <option selected value="{{$product->main_products_id}}">{{$prod->product_name}}.</option>
         @php $prod = App\Main_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         @if($p->id!=$product->main_products_id)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endif
         @endforeach
         @endif
      </select>
   </div>
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('product_name')) }}
   {{ Form::textarea('product_name', $value = $product->product_name ,  $attributes = array('class'=>'form-control ', 'rows'=>'5', 'ng-model'=>'content','id'=>'product_name' ,'placeholder' => 'enter product name')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('description')) }}
   {{ Form::textarea('product_description', $value =$product->product_description ,  $attributes = array('class'=>'form-control ', 'rows'=>'5','id'=>'product_description', 'ng-model'=>'content' ,'placeholder' => 'enter description')) }}
</fieldset>
<fieldset class="form-group">
   {{ Form::label('content', ('Image')) }}
   <input type="file" name="product_image" id="product_image" >
   <img src="<?php echo asset("public/photo/{$product->product_image}")?>" id="profile-img-tag" class="mt-4" width="100px" height="60px">
</fieldset>
<div class="buttons text-center">
   {{Form::submit('Update',['class' => 'btn btn-lg btn-success button senddata' ,])}}
</div>
{!! Form::close() !!}
@endif
@endsection