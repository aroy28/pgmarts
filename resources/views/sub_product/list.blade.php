@extends('layouts.app')
@section('content')
@include('layouts.header2')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-11">
         <div class="navbar-brand" role="group" aria-label="Basic example">
            <div class="Row">
               <div class="Column">
                  <a  href="{{ url('/subproducts/create') }}"><button type="button" style="width: 200" class="btn btn-secondary">New</button></a>
               </div>
               <div class="Column" style="width:200px;">
                  <select class="form-control getsubproduct_by_mainproduct" name="mainproduct_id">
                     <option value="all">Select Main Product</option>
                     @if(count($main_product))
                     @foreach($main_product as $main_product)
                     <option value="{{$main_product->id}}">{{$main_product->product_name}}</option>
                     @endforeach
                     @else
                     no data found
                     @endif
                  </select>
               </div>
               <div class="Column"> </div>
               <div class="Column"> </div>
               <div class="Column"> </div>
            </div>
         </div>
         <div class="card-body" style="background-color: #eee; width: 1140px;height: 480px; border: 2px solid black; overflow: auto;" id="">
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status') }}
            </div>
            @endif
            <div id="show_sub_product">
               @if(count($products)>1)
               @foreach($products as $product)
               <div class="row">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>Product</th>
                           <th> </th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <?php
                              $mainproduct=App\Main_Products::where('id',$product->main_products_id)->first();
                                if (!empty($mainproduct)) {
                              
                              
                                 $mainproduct_name=$mainproduct->product_name;
                                   $mainproduct_id=$mainproduct->id;
                                    # code...
                                }
                                else
                                {
                              
                                     $mainproduct_name='select product';
                                      $mainproduct_id='select product';
                                }      ?>
                           <td class="col-sm-8 col-md-6" style="text-align: center">
                              <div class="media">
                                 <div class="photo">
                                    <a class="thumbnail pull-left" href="subproducts_details/{{$product->id}}" style=""> <img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"> </a>
                                 </div>
                                 <div class="media-body">
                                    <h4  class="product-title" ><a  href="/products/{{$product->id}} ">{{$product->product_name}}</a></h4>
                                    <span>Sub Product :  </span><span class="text-success"><strong>{{$mainproduct_name}}</strong></span>
                                    <span>Description :  </span><span class="text-success"><strong> {{str_limit($product->product_description, 25)}};</strong></span><br>
                                    <!-- <span>Created at :  </span><span class="text-success"><strong>{{$product->created_at}}</strong></span> -->
                                 </div>
                              </div>
                           </td>
                           <td class="col-sm-1 col-md-1" style="text-align: center">
                              <a href="{{url('sub_products_delete', $product->id) }}" class="btn btn-danger">Delete</a>
                              <a href="{{url('sub_products', $product->id) }}" class="btn btn-primary">EDIT</a>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               @endforeach
               {!! $products->render() !!}
               @else
               <h1 class="display-4">Please select Main product </h1>
               @endif
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{ asset('public/js/ajaxcall.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection