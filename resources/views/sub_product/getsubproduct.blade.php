<!-- Button trigger modal -->
<!-- Modal -->
@if(count($products)>0)
@foreach($products as $product)
<div class="row">
   <table class="table table-hover" style="overflow: auto;">
      <thead>
         <tr>
            <th>Product</th>
            <th> </th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <?php
               $mainproduct=App\Main_Products::where('id',$product->main_products_id)->first();
                 if (!empty($mainproduct)) {
               
               
                  $mainproduct_name=$mainproduct->product_name;
                   
                     # code...
                 }
                 else
                 {
               
                      $mainproduct_name='--';
                      
                 }      ?>
            <td class="col-sm-8 col-md-6" style="text-align: center">
               <div class="media">
                  <div class="photo">
                     <a class="thumbnail pull-left" href="subproducts_details/{{$product->id}}" style=""> <img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"> </a>
                  </div>
                  <!-- END OF PHOTO -->
                  <div class="media-body">
                     <h4  class="product-title" ><a  href="/products/{{$product->id}} ">{{$product->product_name}}</a></h4>
                     <span>main Product :  </span><span class="text-success"><strong>{{$mainproduct_name}}</strong></span>
                     <span>Description :  </span><span class="text-success"><strong> {{str_limit($product->product_description, 25)}};</strong></span><br>
                     <!-- <span>Created at :  </span><span class="text-success"><strong>{{$product->created_at}}</strong></span> -->
                  </div>
               </div>
            </td>
            <button>
               <td class="col-sm-1 col-md-1" style="text-align: center">
            <button type="button" class="btn btn-danger deletebutton432" value="{{$product->id}}" >
            Delete
            </button>
            <a href="{{url('sub_products', $product->id) }}" class="btn btn-primary">EDIT</a>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong/{{$product->id}}">
            view
            </button>
            </td>
         </tr>
      </tbody>
   </table>
</div>
<div class="modal fade" id="exampleModalLong/{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Sub product Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body" >
            <div class="preview-pic tab-content">
               <div class="tab-pane active" id="pic-1"><img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"></div>
            </div>
            <h3 class="product-title">{{$product->product_name}}</h3>
            <div class="rating">
               <div class="stars">
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star checked"></span>
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star"></span>
               </div>
            </div>
            <p class="product-description" style="word-wrap: break-word;">{{$product->product_description}}</p>
            <h4 class="price">Main product: <span>{{$mainproduct_name}}</span></h4>
            ...
         </div>
      </div>
   </div>
</div>
@endforeach
@else
<h1 class="display-4">THERE'S NO PRODUCT ON THIS CATEGORY<BR><a href="{{ url('/subproducts/create') }}">ADD PRODUCT </a>  </h1>
@endif
</div>
<script>
   $('.deletebutton432').on('click', function () {
   var id=$(this).val();
   //alert(id);
   
       // return confirm('Are you sure want to delete?');
       event.preventDefault();//this will hold the url
       swal({
           title: "Are you sure?",
           text: "Once clicked, this will be softdeleted!",
           icon: "warning",
           buttons: true,
           dangerMode: true,
       })
       .then((willDelete) => {
           if (willDelete) {
             $.ajax({url: "sub_products_delete/"+id, success: function(result){
               swal("Done! product has been deleted!", {
                   icon: "success",
                   button: false,
               });
           location.reload(true);
                   $('#subproduct_id').html(result);
               }})
              //this will release the event
           } else {
              alert('failed');
               swal("Your imaginary file is safe!");
           }
       });
   });
</script>