@extends('layouts.shop_front')

@section('content')

   <!-- Header -->
    <header class="w3-display-container w3-content w3-wide" style="max-width:1360px; max-height:600px;" id="home">
        <img class="w3-image" src="{{ url('public/images/home.jpg') }}" alt="Architecture" width="1500" height="auto">
        <div class="w3-display-middle w3-margin-top w3-center">
            <h1 class="w3-xxlarge w3-text-white"><span class="w3-padding w3-black w3-opacity-min"><b>Ayushman</b></span> <span class="w3-hide-small w3-text-light-grey">Infotech</span></h1>
        </div>
    </header>
@endsection