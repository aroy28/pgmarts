@extends('layouts.shop');
@extends('layouts.header2');
@section('content')

@php $units = App\Units::all(); @endphp

@if($products_data=='create')
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Create Product')?></h1>
</div>
{!! Form::open(['action' => 'ProductsController@store','method'=>'Post','enctype'=>'multipart/form-data']) !!}
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Sub Product</label>
      <div class="col-sm-8">
         <select class="custom-select my-1 mr-sm-2 cst_select" id="sub_products_id" name="sub_products_id">
         <option selected value="">Choose...</option>
         @php $prod = App\Sub_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endforeach
         @endif
      </select>
      </div>
   </div>
</fieldset>


<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Product name</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="product_name" name="product_name" placeholder="product name">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label"  style="color: green;font-weight: bold;">Product Description</label>
      <div class="col-sm-8">
         <textarea rows = "5" cols = "98" name = "product_description" id="product_description">
        
         </textarea><br>
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Price</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="price" name="amount" placeholder="Amount">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Stock Manage</label>
      <div class="col-sm-8">
         <select class="custom-select my-1 mr-sm-2 cst_select" id="stock_manage" name="stock_manage">
             <option value="">Select</option>
               <option value="in_stock">In Stock</option>
               <option value="out_stock" >Out Of Stock</option>
            </select>
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Quantity</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Units</label>
      <div class="col-sm-8">
         <select class="custom-select my-1 mr-sm-2 cst_select" id="unit" name="unit">
             <option value="">Select</option>
             @foreach($units as $unit)
               <option value="{{$unit->id}}">{{ $unit->unit_name }}</option>
             @endforeach
            </select>
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Brand</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="brand" name="brand" placeholder="Brand">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Key Features</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="key_features" name="key_features" placeholder="Key Features">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Self Life</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="self_life" name="self_life" placeholder="self life">
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Offer</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="offer_rate" name="offer_rate" placeholder="offer rate">
      </div> % discount
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Disclaimer</label>
      <div class="col-sm-8">
         <textarea rows = "5" cols = "98" name = "declaimer" id="declaimer">
       
         </textarea><br>
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Product Image</label>
      <div class="col-sm-8">
         <input type="file" class="form-control" id="product_image" placeholder="product image" name="product_image">
           <!-- <img src="" id="profile-img-tag" class="mt-4" width="100px" height="60px"> -->
      </div>
   </div>
</fieldset>
<div class="form-group row">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary product_data">Submit</button>
   </div>
</div>
</form>
{!! Form::close() !!}

@else


<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Edit Product')?></h1>
</div>
{!! Form::Open(['action'=>['ProductsController@update',$product->id],'method'=>'Post','enctype'=>'multipart/form-data']) !!}
{{method_field('PATCH')}}
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Sub Product</label>
      <div class="col-sm-8">
            <select class="custom-select my-1 mr-sm-2 cst_select" id="sub_products_id" name="sub_products_id">
         @php $prod = App\Sub_Products::where('id',$product->sub_products_id)->first(); @endphp
         <option selected value="{{$product->sub_products_id}}">{{$prod->product_name}}.</option>
         @php $prod = App\Sub_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         @if($p->id!=$product->sub_products_id)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endif
         @endforeach
         @endif
      </select>
      </div>
   </div>
</fieldset>


<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Product name</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="product_name" name="product_name" value="{{$product->product_name}}" placeholder="product name">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label"  style="color: green;font-weight: bold;">Product Description</label>
      <div class="col-sm-8">
         {{ Form::label('content', ('description')) }}
   {{ Form::textarea('product_description', $value =$product->product_description ,  $attributes = array('class'=>'form-control ', 'rows'=>'5','id'=>'product_description', 'ng-model'=>'content' ,'placeholder' => 'enter description')) }}
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Price</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="price" name="amount" placeholder="Amount" value="{{$product->amount}}">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Stock Manage</label>
      <div class="col-sm-8">
         <select class="custom-select my-1 mr-sm-2 cst_select" id="stock_manage" name="stock_manage">
             <option value="">Select</option>
               @if($product->stock_manage=="in_stock")
         <option value="in_stock" selected="" >In Stock</option>
         <option value="out_stock" >Out Of Stock</option>
         @elseif($product->stock_manage=="out_stock")
         <option value="out_stock" selected="">Out Of Stock</option>
         <option value="in_stock" >In Stock</option>
         @else
         <option value="">Select Stock</option>
         <option value="in_stock">In Stock</option>
         <option value="out_stock" >Out Of Stock</option>
         @endif
   
            </select>
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Quantity</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="quantity" value="{{$product->quantity}}" name="quantity" placeholder="Quantity">
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Units</label>
      <div class="col-sm-8">
         <select class="custom-select my-1 mr-sm-2 cst_select" id="unit" name="unit">
             <option value="">Select</option>
             @foreach($units as $unit)
               @php $selected=""; if($product->unit==$unit->id){ $selected="selected"; } @endphp
               <option value="{{ $unit->id }}" <?php echo $selected; ?> >{{ $unit->unit_name }}</option>
             @endforeach
            </select>
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Brand</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="brand" name="brand" value="{{$product->brand}}" placeholder="Brand">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Key Features</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" id="key_features" value="{{$product->key_features}}" name="key_features" placeholder="Key Features">
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Self Life</label>
      <div class="col-sm-8">
         <input type="text" class="form-control" value="{{$product->self_life}}" id="self_life" name="self_life" placeholder="self life">
      </div>
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Offer</label>
      <div class="col-sm-8">
         <input type="number" class="form-control" id="offer_rate" name="offer_rate" value="{{$product->offer_rate}}" placeholder="offer rate">
      </div> % discount
   </div>
</fieldset>

<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Disclaimer</label>
      <div class="col-sm-8">
         {{ Form::label('content', ('Disclaimer')) }}
   {{ Form::textarea('declaimer', $value =$product->declaimer ,  $attributes = array('class'=>'form-control ', 'rows'=>'5','id'=>'declaimer', 'ng-model'=>'content' ,'placeholder' => 'enter description')) }}
      </div>
   </div>
</fieldset>
<fieldset class="form-group">
   <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label" style="color: green;font-weight: bold;">Product Image</label>
      <div class="col-sm-8">
         <input type="file" class="form-control" id="" value="{{$product->product_image}}" placeholder="product image" name="product_image">
          
            <img src="<?php echo asset("/public/photo/{$product->product_image}")?>" id="profile-img-tag" class="mt-4" width="100px" height="60px">
      </div>
   </div>
</fieldset>
<div class="form-group row">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary product_data">Update</button>
   </div>
</div>
</form>
{!! Form::close() !!}

@endif
@endsection