@extends('layouts.shop');
@extends('layouts.header2');
@section('content')
<!-- <div class="navbar-brand" role="group" aria-label="Basic example">
   <a href="{{ url('/home') }}"><button type="button" style="width: 100" class="btn btn-secondary">Product List</button></a>
</div> -->
@if($products_data=='create')
<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Create Product')?></h1>
</div>
{!! Form::open(['action' => 'ProductsController@store','method'=>'Post','enctype'=>'multipart/form-data']) !!}

<fieldset class="form-group">
   <label for="colFormLabel" class="col-sm-3 col-form-label">Sub Product <span class="start_color">*</span></label>
   <div class="col-sm-14">
      <select class="custom-select my-1 mr-sm-2 cst_select" id="sub_products_id" name="sub_products_id">
         <option selected value="">Choose...</option>
         @php $prod = App\Sub_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endforeach
         @endif
      </select>
   </div>
</fieldset>
{{Form::label('product_name','PRODUCT TITLE',['class' => '','id'=>'product_name'])}}
{{Form::text('product_name','',['class'=>'form-control','id'=>'product_name','placeholder'=>'MAX 50 CHARACTERS'])}}
{{Form::label('product_description','PRODUCT DESCRIPTION',['class' => '','id'=>'product_description'])}}
{{Form::textarea('product_description','',['class'=>'form-control rounded-0','id'=>'product_description','placeholder'=>'MAX 250 CHARACTERS'])}}
<div class="row" class="col-md-4">
   <div class="col-md-4">
      {{Form::label('amount','PRICE',['class' => ''])}}
      {{Form::number('amount','',['class'=>'form-control','placeholder'=>'SET A PRICE'])}}
   </div>
   <div class="col-md-4">
      {{Form::label('quantity','QUANTITY',['class' => ''])}}
      {{Form::number('quantity','',['class'=>'form-control','placeholder'=>'SET QUANTITY'])}}
   </div>
      <div class="col-md-12">
      {{Form::label('brand','brand',['class' => ''])}}
      {{Form::text('brand','',['class'=>'form-control','placeholder'=>'SET brand'])}}
   </div>
      <div class="col-md-12">
      {{Form::label('key_features','',['class' => ''])}}
      {{Form::text('key_features','',['class'=>'form-control','placeholder'=>'SET key features'])}}
   </div>

      <div class="col-md-12">
      {{Form::label('self_life','self life',['class' => ''])}}
      {{Form::number('self_life','',['class'=>'form-control','placeholder'=>'SET self life'])}}
   </div>
      <div class="col-md-12">
      {{Form::label('declaimer','declaimer',['class' => ''])}}
      {{Form::textarea('declaimer','',['class'=>'form-control','placeholder'=>'SET declaimer'])}}
   </div>
   <div class="col-md-4">
      <fieldset>
         <label>Stock Manage <span class="start_color">*</span></label>
         <div class="col-sm-14">
            <select class="custom-select my-1 mr-sm-2 cst_select" id="stock_manage" name="stock_manage">
               <option value="in_stock">In Stock</option>
               <option value="out_stock" >Out Of Stock</option>
            </select>
         </div>
      </fieldset>
   </div>
   <div class="col-md-4">
      {{Form::label('product_image','IMAGE',['class' => ''])}}
      {{Form::file('product_image')}}
      <img src="" id="profile-img-tag" class="mt-4" width="100px" height="60px">
   </div>
   <div class="col-md-4">
   </div>
</div>
{{Form::submit('ADD',['class' => 'btn btn-primary senddata'])}}
{!! Form::close() !!}
@else

<div id="page-title">
   <h1 class="page-header text-overflow"><?php echo('Edit Products')?></h1>
</div>

{!! Form::Open(['action'=>['ProductsController@update',$product->id],'method'=>'Post','enctype'=>'multipart/form-data']) !!}
{{method_field('PATCH')}}


<fieldset class="form-group">
   <label for="colFormLabel" class="col-sm-3 col-form-label">Edit Sub Product <span class="start_color">*</span></label>
   <div class="col-sm-14">
      <select class="custom-select my-1 mr-sm-2 cst_select" id="sub_products_id" name="sub_products_id">
         @php $prod = App\Sub_Products::where('id',$product->sub_products_id)->first(); @endphp
         <option selected value="{{$product->sub_products_id}}">{{$prod->product_name}}.</option>
         @php $prod = App\Sub_Products::All(); @endphp
         @if(count($prod))
         @foreach($prod as $p)
         @if($p->id!=$product->sub_products_id)
         <option value="{{$p->id}}">{{$p->product_name}}</option>
         @endif
         @endforeach
         @endif
      </select>
   </div>
</fieldset>
<fieldset class="form-group">
   <label for="colFormLabel" class="col-sm-3 col-form-label">Stock Manage <span class="start_color">*</span></label>
   <div class="col-sm-14">
      <select class="custom-select my-1 mr-sm-2 cst_select" id="stock_manage" name="stock_manage">
         @if($product->stock_manage=="in_stock")
         <option value="{{$product->stock_manage}}" selected="" >In Stock</option>
         <option value="{{$product->stock_manage}}" >Out Of Stock</option>
         @elseif($product->stock_manage=="out_stock")
         <option value="{{$product->stock_manage}}" selected="">Out Of Stock</option>
         <option value="{{$product->stock_manage}}" >In Stock</option>
         @else
         <option value="">Select Stock</option>
         <option value="in_stock">In Stock</option>
         <option value="out_stock" >Out Of Stock</option>
         @endif
      </select>
   </div>
</fieldset>
{{Form::label('product_name','PRODUCT TITLE',['class' => ''])}}
{{Form::text('product_name',$product->product_name,['class'=>'form-control','placeholder'=>'MAX 50 CHARACTERS'])}}
{{Form::label('product_description','PRODUCT DESCRIPTION',['class' => ''])}}
{{Form::textarea('product_description',$product->product_description,['class'=>'form-control rounded-0','placeholder'=>'MAX 250 CHARACTERS'])}}
<div class="row">
   <div class="col-md-4">
      {{Form::label('amount','PRICE',['class' => ''])}}
      {{Form::number('amount',$product->amount,['class'=>'form-control','placeholder'=>'SET A PRICE'])}}
   </div>
   <div class="col-md-4">
      {{Form::label('quantity','QUANTITY',['class' => ''])}}
      {{Form::number('quantity',$product->quantity,['class'=>'form-control','placeholder'=>'SET QUANTITY'])}}
   </div>
    <div class="col-md-12">
      {{Form::label('brand','brand',['class' => ''])}}
      {{Form::text('brand',$product->brand,['class'=>'form-control','placeholder'=>'SET brand'])}}
   </div>
      <div class="col-md-12">
      {{Form::label('key_features','',['class' => ''])}}
      {{Form::text('key_features',$product->key_features,['class'=>'form-control','placeholder'=>'SET key features'])}}
   </div>
   
      <div class="col-md-12">
      {{Form::label('self_life','self life',['class' => ''])}}
      {{Form::number('self_life',$product->self_life,['class'=>'form-control','placeholder'=>'SET self life'])}}
   </div>
      <div class="col-md-12">
      {{Form::label('declaimer','declaimer',['class' => ''])}}
      {{Form::textarea('declaimer',$product->declaimer,['class'=>'form-control','placeholder'=>'SET declaimer'])}}
   </div>
   <div class="col-md-4">
      {{Form::label('product_image','IMAGE',['class' => ''])}}
      {{Form::file('product_image')}}
      <img src="<?php echo asset("/public/photo/{$product->product_image}")?>" id="profile-img-tag" class="mt-4" width="100px" height="60px">
   </div>
</div>
{{Form::submit('Update',['class' => 'btn btn-primary senddata'])}}
{!! Form::close() !!}
@endif
@endsection