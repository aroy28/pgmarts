  @if(count($products)>0)
            
            <div class="row" >
               <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Product List</th>
                        <th class="text-center"></th>
                        <th> </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($products as $product)
                     <tr>
                        <td class="col-sm-8 col-md-6" style="text-align: center">
                           <div class="media">
                              <div class="photo">
                                 <a class="thumbnail pull-left" href="products/{{$product->id}}" style=""> <img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"> </a>
                              </div>
                              <!-- END OF PHOTO -->
                              <?php
                                 $sub_product=App\Sub_Products::where('id',$product->sub_products_id)->first();


                               if (!empty($sub_product)) {

                               $subproduct=$sub_product->product_name;
            # code...
                                  }

                                 else
                                {

                              $subproduct='--';

                               }
      
         
         
                            $mainproduct=App\Main_Products::where('id',$sub_product->main_products_id)->first();


                           if (!empty($mainproduct)) {

                            $mainproduct=$mainproduct->product_name;
            # code...
                            }
       
                               else
                             {

                           $mainproduct='--';

                              } 
                              ?>
                              <div class="media-body">
                                 <a  href="/products/{{$product->id}} ">{{$product->product_name}}</a>

                                 <!-- <span>Main Product :  </span><span class="text-success"><strong></strong></span><br>

                                 <span>Sub Product :  </span><span class="text-success"><strong></strong></span><br> -->

                                 <!-- <span>Created at :  </span><span class="text-success"><strong>{{$product->created_at}}</strong></span> -->
                              </div>
                              <div class="media-body">
                                 <strong>  {{$product->amount}}.00</strong>
                              </div>
                              <div class="media-body">
                                 <a  href="/products/{{$product->id}} ">{{$product->stock_manage}}</a>
                              </div>
                           </div>
                        </td>
                        
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                           <button type="button" class="btn btn-danger deletebutton43211" value="{{$product->id}}" >
                           Delete
                           </button>
                           <a href="{{url('products_edit', $product->id) }}" class="btn btn-primary ">EDIT</a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong2/{{$product->id}}">
 view
</button>
                        </td>
                     </tr>
                         <div class="modal fade" id="exampleModalLong2/{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sub product Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
             <div class="preview-pic tab-content">
                     <div class="tab-pane active" id="pic-1"><img src = "<?php echo asset("/public/photo/{$product->product_image}")?>"></div>

                 </div>



           
                 <h3 class="product-title">{{$product->product_name}}</h3>
                 <div class="rating">
                     <div class="stars">
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                         <span class="fa fa-star"></span>
                     </div>
                    
                 </div>
                 <p class="product-description" style="word-wrap: break-word;">{{$product->product_description}}</p>
                    <h4 class="price">Price: <span>${{$product->amount}}</span></h4>
                     <h4 class="price">Quantity: <span>{{$product->quantity}}</span></h4>
                 <h4 class="price">Main product: <span>{{$mainproduct}}</span></h4>
                  <h4 class="price">Sub product: <span>{{$subproduct}}</span></h4>
                    <h4 class="price">Key features: <span>{{$product->key_features}}</span></h4>
                      <h4 class="price">Self life: <span>{{$product->self_life}}</span></h4>
                        <h4 class="price">Brand: <span>{{$product->brand}}</span></h4>
                           <h4 class="price">Disclaimer: <span>{{$product->declaimer}}</span></h4>
               
            
        ...
      </div>
     
    </div>
  </div>
</div>
                     @endforeach
                  </tbody>
               </table>
            </div>
            &nbsp;&nbsp;&nbsp;
          
            @else
           <h1 class="display-4">THERE'S NO PRODUCT ON THIS CATEGORY<BR><a href="{{ url('/products/create') }}">ADD PRODUCT </a>  </h1>
            @endif
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                <script>
    $('.deletebutton43211').on('click', function () {
   var id=$(this).val();
   //alert(id);

        // return confirm('Are you sure want to delete?');
        event.preventDefault();//this will hold the url
        swal({
            title: "Are you sure?",
            text: "Once clicked, this will be deleted!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
              $.ajax({url: "prod_del/"+id, success: function(result){
                swal("Done! product has been deleted!", {
                    icon: "success",
                    button: false,
                });
            location.reload(true);
                    $('#subproduct_id').html(result);
                }})
               //this will release the event
            } else {
               alert('failed');
                swal("Your imaginary file is safe!");
            }
        });
    });
</script>


            