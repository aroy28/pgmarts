
<html lang="en">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- select2 library for css -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
       <meta name="csrf-token" content="{{ csrf_token() }}">



    <title>Easy Cart Portal</title>
    <style>
        .set-challenge-block {
            display: inherit;
            margin: 0 auto;
            padding: 5px;
        }
        input {
            text-align: center;}
            .bg-light-customized{background-color:#3792cb;}
            .nav-link-customised{font-size:17px;color:#dadada!important;font-weight:500;}
            body{background-color:#F8F8F8!important;}
             .logincss{text-align:center;font-weight:600;}
             .logincss-1{text-align:center;font-size:18px;}
             .card-boxshadow{box-shadow:2px 2px 2px 2px #eae8e8; }
            @media screen and (min-device-width: 320px) and (max-device-width: 500px){
               .nav-item{text-align:center;}
        }
        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }
        .Column {
            display: table-cell;
            /*background-color: red; /*Optional*/*/
        }
    </style>
  </head>
  <body>
    <!--Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light-customized">
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
               
                    @if(Auth::user()->role=='1')
                    <li class="nav-item">
                        <a class="nav-link nav-link-customised waves-effect waves-light" href="{{ url('/mainproducts') }}" @if(isset($active_class) &&
                    $active_class=='mainproduct' ) style="background: #008000 !important;" @endif>Main product</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link nav-link-customised waves-effect waves-light" href="{{ url('/subproducts') }}"  @if(isset($active_class) &&
                    $active_class=='subproduct' ) style="background: #008000 !important;" @endif>Sub Product </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link nav-link-customised waves-effect waves-light" href="{{ url('/home') }}"  @if(isset($active_class) &&
                    $active_class=='product' ) style="background: #008000 !important;" @endif>Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-customised waves-effect waves-light" href="{{ route('user_list') }}"  @if(isset($active_class) &&
                    $active_class=='user-list' ) style="background: #008000 !important;" @endif>Users</a>
                    </li>
                    

                    @endif
                    
                    @if(Auth::user()->role=='3' || Auth::user()->role=='1') 
                    <li class="nav-item">
                        <a class="nav-link nav-link-customised waves-effect waves-light" href="{{ route('deliver-boy-orders') }}"  @if(isset($active_class) &&
                    $active_class=='order-list' ) style="background: #008000 !important;" @endif>Orders</a>
                    </li>
                    @endif

            </ul>
            <ul class="navbar-nav ml-auto">
                <li> <a class="navbar-brand">Welcome {{ Auth::user()->name }}</a> </li>
                <li class="nav-item">
                   <a class="nav-link nav-link-customised waves-effect waves-light" href=" {{ route('logout') }}">Logout</a>
                </li> 
            </ul>
           
        </div>
    </nav>

     
    <!--Navbar end-->
    </body>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- select2 library for js -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> -->
    </html>
    