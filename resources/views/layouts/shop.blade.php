<html>
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <head>
      <title>PRODUCTS</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
      <link href="{{ asset('public/css/shop.css') }}" rel="stylesheet">

   </head>
   <body>
       <meta name="csrf-token" content="{{ csrf_token() }}">

      <div id="app">
         <div class="container">
            @include('inc.shopnav')
            @yield('content')
         </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
      <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs/dist/tf.min.js"> </script>

      <script type="application/javascript">
         $(".senddata").click(function() {
          var main_products_id = document.getElementById('main_products_id');

          var sub_products_id = document.getElementById('sub_products_id');

          var main_product_name = document.getElementById('main_products_id');

          var product_name = document.getElementById('product_name');

          var product_description = document.getElementById('product_description');

          if (main_products_id.value == "0") {


              swal('error', 'please select main product', 'error');

              return false;
          }


          if (sub_products_id.value == "") {


              swal('error', 'please select sub product', 'error');

              return false;
          }


          if (main_product_name.value == "") {


              swal('error', 'please enter main product', 'error');

              return false;
          }


          if (product_name.value == "") {


              swal('error', 'please enter product name', 'error');

              return false;
          }

          if (product_description.value == "") {


              swal('error', 'please enter product description', 'error');

              return false;
          }

      });


      $(".product_data").click(function() {

          var sub_products_id = document.getElementById('sub_products_id');
          var product_name = document.getElementById('product_name');
          var product_description = document.getElementById('product_description');
          var price = document.getElementById('price');
          var stock_manage = document.getElementById('stock_manage');
          var quantity = document.getElementById('quantity');
          var brand = document.getElementById('brand');
          var key_features = document.getElementById('key_features');
          var self_life = document.getElementById('self_life');
          var declaimer = document.getElementById('declaimer');
          var product_image = document.getElementById('product_image');

          if (sub_products_id.value == "") {


              swal('error', 'please select sub product ', 'error');

              return false;
          }


          if (product_name.value == "") {


              swal('error', 'please enter product name', 'error');

              return false;
          }

          if (product_description.value == "") {


              swal('error', 'please enter product description', 'error');

              return false;
          }

          if (price.value == "") {


              swal('error', 'please enter product price', 'error');

              return false;
          }

          if (stock_manage.value == "") {


              swal('error', 'please select  stock manage', 'error');

              return false;
          }

          if (quantity.value == "") {


              swal('error', 'please enter quantity', 'error');

              return false;
          }

          if (brand.value == "") {


              swal('error', 'please enter brand', 'error');

              return false;
          }

          if (key_features.value == "") {


              swal('error', 'please enter product key features', 'error');

              return false;
          }

          if (self_life.value == "") {


              swal('error', 'please enter product self life', 'error');

              return false;
          }

          if (declaimer.value == "") {


              swal('error', 'please enter product disclaimer', 'error');

              return false;
          }

          if (product_image.value == "") {


              swal('error', 'please select product image', 'error');

              return false;
          }

      });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#product_image").change(function() {
            readURL(this);
        }); 

        $(document).ready(function() {
            // alert("yes");
            $(".main_products_id").on('change', function() {
                //alert('hfhf');
                var id = $(this).val();
                //alert("fetch_subproduct/"+id);
                //alert(id);
                $.ajax({
                   headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "fetch_subproduct/" + id,
                    success: function(result) {
                        //alert(result);
                        $('#sub_products_id').html(result);
                    }
                })
            });
        });   
      </script>
   </body>
</html>