<html>
<head>
    <title>PRODUCTS</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="{{ asset('public/css/shop.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/w3.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <!-- <div class="flex-center position-ref full-height"> -->
    <div class="w3-top">
        <div class="w3-bar w3-white w3-wide w3-padding w3-card">
            <!-- <a href="{{ url('/') }}" class="w3-bar-item w3-button"><b>Online</b> Shopping</a> -->
            <a href="{{ url('/') }}" class="w3-bar-item w3-button">
              <img class="w3-image" src="{{ asset('public/images/logo.png') }}" alt="Architecture">
            </a>
            @if (Route::has('login'))
            <!-- <div class="top-right links"> -->
                @auth
                    <a href="{{ url('/') }}">
                        <div class="container">
                            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                                <a class="navbar-brand" href="">Dashboard</a>
                            </nav>
                        </div>
                    </a>
                @else
                    <div class="w3-right w3-hide-small">
                      <a href="{{ route('login') }}" class="w3-bar-item w3-button">Login</a>
                      <a href="{{ route('register') }}" class="w3-bar-item w3-button">Register</a>
                    </div>
                @endauth
            @endif
        </div>
        

        <!-- <div id="app">
            <div class="container"> -->
                @yield('content')
            <!-- </div>
        </div> -->
    </div>
</body>
</html>