<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
         <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}" crossorigin="anonymous">

      <!-- <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Styles -->
      <link href="{{ asset('public/css/shop.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
   <!--    <link href="{{ asset('css/w3.css') }}" rel="stylesheet">///create conflict to open bootstrap modal
 -->   </head>
   <body>
      <div class="w3-top">
      <!-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
         <div class="container">
            @guest
            <div class="w3-right w3-hide-small">
               <a class="w3-bar-item w3-button" href="{{ url('/') }}">
               HOME
               </a>
               <div class="navbar-brand" role="group" aria-label="Basic example">
                  <a  href="{{ url('/products/create') }}"><button type="button" style="width: 100" class="btn btn-secondary">Product List</button></a>
               </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
               </ul>
               <ul class="navbar-nav ml-auto">
               <div class="w3-right w3-hide-small">
                  <a class="w3-bar-item w3-button" href="{{ route('login') }}">{{ __('Login') }}</a>
                  <a class="w3-bar-item w3-button" href="{{ route('register') }}">{{ __('Register') }}</a>
               </div>
               @else
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                  </ul>
                  <ul class="navbar-nav ml-auto">
                     @endguest
                  </ul>
               </div>
            </div>
      </nav> -->
      @include('inc.warning')
      <main class="py-4">
      @yield('content')
      </main>
      </div>
      <!-- Scripts -->
       <script src="{{ asset('public/js/jquery.min.js') }}"></script>
      <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('public/js/app.js') }}"></script>
   </body>
</html>