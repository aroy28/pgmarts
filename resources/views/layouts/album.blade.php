<html>
   <head>
      <title>PRODUCTS</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
      <link href="{{ asset('public/css/shop.css') }}" rel="stylesheet">
      <meta name="csrf-token" content="{{ csrf_token() }}">
   </head>
   <body>
      <div id="app">
         <div class="container">
            @include('inc.warning')
            @yield('content')
         </div>
      </div>
         <script src="{{ asset('public/js/dropzone.js') }}"></script>
   </body>
</html>