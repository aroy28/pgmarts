@extends('layouts.app')
@section('content')
@include('layouts.header2')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-11">
         <div class="navbar-brand" role="group" aria-label="Basic example">
         </div>
         <div class="card-body" style="background-color: #eee; width: 1140px;height: 490px; border: 2px solid black; overflow: auto;" id="">
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status') }}
            </div>
            @endif
            @if(count($orders)>0)
            <div class="row">
               <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Order Details</th>
                        <th> </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($orders as $order)
                     <tr>
                        <td class="col-sm-8 col-md-6" style="text-align: center">
                           <div class="media">
                              <div class="photo">
                              </div>
                              <!-- END OF PHOTO -->
                              <div class="media-body">
                                <h4 class="text-success">Order</h4>
                                <span>Order ID :  </span><span class="text-success"><strong>#{{$order->id?$order->id:'NULL'}}</strong></span>
                                <br>
                                  <span>Status :  </span><span class="text-success">{{$order->order_status}}</span>
                                  <br>
                                 <span>Grand Total :  </span>
                                 <span class="text-success" ><strong> {{$order->grand_total}}</strong></span>
                                 <br>
                                 <span>Order Date :  </span><span class="text-success" ><strong>{{$order->order_date}}</strong></span>
                                 <br>
                                 <span>Delivery Date :  </span><span class="text-success"><strong>{{$order->delivery_date?$order->delivery_date:'NULL'}}</strong></span>
                                 <br>

                                 <span>Delivery Charge :  </span><span class="text-success"><strong>{{$order->delivery_charge?$order->delivery_charge:'NULL'}}</strong></span>
                                 <span>Payment Type :  </span><span class="text-success"><strong>{{$order->pay_via?$order->pay_via:'NULL'}}</strong></span>

                                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong1/{{$order->id}}">
                                 Change Status
                                  </button>
                              </div>
                           </div>
                        </td>
                     </tr>

                     <div class="modal fade" id="exampleModalLong1/{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h5 class="modal-title" id="exampleModalLongTitle">Order Status</h5>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="modal-body" >
                                 <form action="" method="post" enctype="multipart/form-data">
                                  <input type="hidden" name="orderid" id="orderid" value="{{$order->id}}">
                                   <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                                   <input type="hidden" name="changeorderstatusurl" id="changeorderstatusurl" value="{{route('change_order_status')}}">
                                 <h4 class="product-title">Select Status</h4>
                                 <select class="" name="order_status" id="order_status">
                                    <option value="">SELECT STATUS</option>
                                    @foreach($statuses as $stats)
                                    <option value="{{$stats->id}}" @php if($order->order_status == $stats->name) { echo 'selected'; } @endphp >{{$stats->name}}</option>
                                    @endforeach
                                 </select>

                                 <br/><br/>
                                 <div >
                                  <p class="">Enter Remarks below</p>
                                   <input type="text" name="order_remarks" id="order_remarks" value="">
                                 </div>
                                  <button class="btn btn-primary" id="saving" style="display:none;">Saving...
                                  </button>

                                 <button type="button" class="btn btn-primary savestatus" >
                                 Save
                                  </button>
                                </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach


                     <tr>
                       <td class="col-sm-8 col-md-6" style="text-align: center">
                           <div class="media">
                                
                              <!-- END OF PHOTO -->
                              <div class="media-body">
                                <h4 class="text-success">Product Details</h4>
                                @foreach($orderedproducts as $val )
                                <span>Product Name :  </span><span class="text-success"><strong>{{$val['product_name']?$val['product_name']:'NULL'}}</strong></span>
                                <br>
                                  <span>Description :  </span><span class="text-success">{{$val['product_description']?$val['product_description']:'NULL'}}</span>
                                  <br>
                                 <span>Qty :  </span>
                                 <span class="text-success" ><strong>{{$val['quantity']?$val['quantity']:'NULL'}}</strong></span>
                                 <br>
                                 <span>Price :  </span><span class="text-success" ><strong>{{$val['product_price']?$val['product_price']:'NULL'}}</strong></span>
                                 <br>
                                 <span>Product Id:  </span><span class="text-success"><strong>{{$val['product_id']?$val['product_id']:'NULL'}}</strong></span>
                                 <br>
                                 @endforeach


                              </div>
                           </div>
                     </tr>

                  </tbody>
               </table>
            </div>
            @else
            <h1 class="display-4">THERE'S NO ORDER<BR></h1>
            @endif
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $('.savestatus').on('click', function () {
      var orderid=$('#orderid').val();
      var token = $('#token').val();
      var order_remarks = $('#order_remarks').val();
      var changeorderstatusurl = $('#changeorderstatusurl').val();
      var order_status = $('#order_status').val();
      if(order_status =='' || order_status==null){
        alert('Select status'); return false;
      }

       $('#saving').show();
       $(this).hide();

              $.ajax({
                url: changeorderstatusurl, 
                method:'post',
                data:{'_token':token ,'orderid':orderid,'status':order_status },
                dataType:'Json',
                success: function(result){

                  $('#saving').hide();
                  $(this).show();
                  
                  if(result.status==1){
                    swal("Done! product has been deleted!", {
                        icon: "success",
                        button: false,
                    });
                    location.reload(true);
                  } else {
                    swal("Error! Status did not change!", {
                        icon: "error",
                        button: false,
                    });
                    location.reload(true);
                  }
                
                  
                }})
               //this will release the event
           
    });
</script>
         </div>
      </div>
   </div>
</div>

@endsection