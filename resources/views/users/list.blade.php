@extends('layouts.app')
@section('content')
@include('layouts.header2')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-11">
         <div class="navbar-brand" role="group" aria-label="Basic example">
         </div>
         <div class="card-body" style="background-color: #eee; width: 1140px;height: 490px; border: 2px solid black; overflow: auto;" id="">
            @if (session('addusersucessmsg'))
            <div class="alert alert-success">
               {{ session('addusersucessmsg') }}
            </div>
            @endif

            @if (session('deletemsg'))
            <div class="alert alert-success">
               {{ session('deletemsg') }}
            </div>
            @endif
            @if(count($users)>0)
            <div class="row">
               <table class="table table-hover">
                  <thead>
                     <tr>
                        <th>Users List <a href="{{route('add_user')}}" class="btn btn-link" >Add User</a></th>
                        <th> </th>
                     </tr>
                     <form method="get" action="">
                        
                         <div class="form-group row">
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="name" value="{{$name}}" placeholder="Name">
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="email" value="{{$email}}" placeholder="Email">
                            </div>
                            <div class="col-md-2">
                                <select class="" name="role" class="form-control" id="role" >
                                   <option value="" >Select Role</option>
                                    @foreach($roles as $key=>$val)
                                    <option @php if($key==$searchrole){ echo "selected"; } @endphp value="{{$key}}" >{{$val}}</option>
                                    @endforeach
                                 </select>
                            </div>

                            <div class="col-md-2">
                                <select class="" name="status" class="form-control" id="status" >
                                   <option value="" >Select status</option>
                                    <option <?php if($status=='1'){ echo "selected"; } ?> value="1" >Active</option>
                                    <option @php if($status=='2'){ echo "selected"; } @endphp value="2" >Deleted</option>
                                 </select>
                            </div>
                            <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">
                                   Search
                                </button>
                            </div>
                        </div>
                    </form>
                  </thead>
                  <tbody>
                     @foreach($users as $user)
                     @php $role=$user->role;  @endphp
                     <tr>
                        <td class="col-sm-8 col-md-6" style="text-align: center">
                           <div class="media">
                              <div class="photo">
                                 <a class="thumbnail pull-left" href="#" style=""> <img src = "<?php //echo asset("/public/photo/{$product->product_image}")?>"> </a>
                              </div>
                              <!-- END OF PHOTO -->
                              <div class="media-body">
                                <span>User ID :  </span><span class="text-success"><strong>#{{$user->id?$user->id:'NULL'}}</strong></span>
                                <span>Name:  </span><span class="text-success"><strong>{{$user->name?$user->name:'NULL'}}</strong></span>
                                 <h4  class="product-title" >@php foreach($roles as $key=>$val){
                                    if($key==$role){
                                     echo $val;
                                   }
                                 } @endphp</h4>
                                 <span>Email :  </span><span class="text-success"><strong> {{$user->email}}</strong></span><br>
                                 <span>Created Date :  </span><span class="text-success"><strong>{{$user->created_at}}</strong></span>
                                 <span>Phone :  </span><span class="text-success"><strong>{{$user->phone?$user->phone:'NULL'}}</strong></span>

                                 <span>Status:  </span><span class="text-success"><strong>{{$user->status==1?"Active":'Inactive'}}</strong></span>
                              </div>
                           </div>
                        </td>
                        <td class="col-sm-1" style="text-align: center"> 
                         @if($user->status=='1')   
                         <a href="{{ route('delete_user', $user->id) }}" class="btn btn-danger" >Delete</a>
                         @endif
                         @if($user->status=='2')
                         <a href="{{ route('restore_user', $user->id) }}" class="btn btn-danger" >Restore</a>
                         @endif
                           <a href="{{ route('edit_user', $user->id) }}" class="btn btn-dange text-success" >Edit</a>
                                
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            {!! $users->render() !!}
            @else
            <h1 class="display-4">THERE'S NO USERS<BR></h1>
            @endif
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $('.savestatus').on('click', function () {
      var orderid=$('#orderid').val();
      var token = $('#token').val();
      var changeorderstatusurl = $('#changeorderstatusurl').val();
      var order_status = $('#order_status').val();
      console.log(order_status);
      if(order_status =='' || order_status==null){
        alert('Select status'); return false;
      }
              $.ajax({
                url: changeorderstatusurl, 
                method:'post',
                data:{'_token':token ,'orderid':orderid,'status':order_status },
                dataType:'Json',
                success: function(result){
                  if(result.status==1){
                    swal("Done! product has been deleted!", {
                        icon: "success",
                        button: false,
                    });
                    location.reload(true);
                  } else {
                    swal("Error! Status did not change!", {
                        icon: "error",
                        button: false,
                    });
                    location.reload(true);
                  }
                
                  
                }})
               //this will release the event
           
    });
</script>
         </div>
      </div>
   </div>
</div>

@endsection