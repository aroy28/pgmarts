<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'ORDER_STATUS' => [
        '1'=>'ordered', 
        '2'=>'pending', 
        '3'=>'failed'
    ],

    'status' => [
        'ordered'=>1, 
        'pending'=>2, 
        'failed'=>3,
        'cancelled'=>4, 
        'refunded'=>5, 
        'onhold'=>6,
        'processing'=>7, 
        'pendingpayment'=>8, 
        'shipped'=>9
    ],

    'role' => [
        '1'=>'admin', 
        '2'=>'user', 
        '3'=>'delivery_boy',
        '4'=>'vendor'
    ],
    'RAZOR_SECRET' => env('RAZOR_SECRET',null),

    'SMS_USERNAME' => env('SMS_USERNAME','anikroy@pgmarts.com'),
    'SMS_HASH' => env('SMS_HASH','230588765477dc57c900b6f1a26b6f90968449f58f7027ba04f87491bde4ce7a'),
    'SMS_SENDER' => env('SMS_SENDER','PGMARTS'),
    'SMS_TEMPLATE' => env('SMS_TEMPLATE',"Use var_otp as your verification code on Pgmart. Team Pgmart"),


];
