<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   

    	$data =[
    		[
				'id' => config('constants.status.completed'),
				'name' => 'Completed'
    		],
    		[
				'id' => config('constants.status.pending'),
				'name' => 'Pending'
    		],
    		[
				'id' => config('constants.status.failed'),
				'name' => 'Failed'
    		],
    		[
				'id' => config('constants.status.cancelled'),
				'name' => 'Cancelled'
    		],
    		[
				'id' => config('constants.status.refunded'),
				'name' => 'Refunded'
    		],
    		[
				'id' => config('constants.status.onhold'),
				'name' => 'On hold'
    		],
    		[
				'id' => config('constants.status.processing'),
				'name' => 'Processing'
    		],
    		[
				'id' => config('constants.status.pendingpayment'),
				'name' => 'Pending payment'
    		],
    		[
				'id' => config('constants.status.shipped'),
				'name' => 'Shipped'
    		]
    	];

    	DB::table('status')->truncate();
        DB::table('status')->insert($data);
    }
}
