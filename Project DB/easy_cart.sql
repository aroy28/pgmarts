-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2020 at 06:32 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easy_cart`
--

-- --------------------------------------------------------

--
-- Table structure for table `main_products`
--

CREATE TABLE `main_products` (
  `id` int(11) NOT NULL,
  `product_name` text CHARACTER SET latin1,
  `product_description` text CHARACTER SET latin1,
  `product_image` text CHARACTER SET latin1,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `main_products`
--

INSERT INTO `main_products` (`id`, `product_name`, `product_description`, `product_image`, `created_at`, `updated_at`) VALUES
(17, 'Milk product', 'best milk in india', '1602260055.jpg', '2020-10-09 10:44:15', '2020-10-09 10:44:15'),
(18, 'Fruits Item', 'best item in india', '1602260082.jpg', '2020-10-09 10:44:42', '2020-10-09 10:44:42'),
(19, 'Accessories product', 'best item in india', '1602260111.jpg', '2020-10-09 10:45:12', '2020-10-09 10:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_02_113457_create_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `stock_manage` enum('in_stock','out_stock') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'out_stock',
  `sub_products_id` int(110) DEFAULT NULL,
  `brand` text COLLATE utf8mb4_unicode_ci,
  `key_features` text COLLATE utf8mb4_unicode_ci,
  `self_life` text COLLATE utf8mb4_unicode_ci,
  `declaimer` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `amount` double(18,2) DEFAULT NULL,
  `quantity` int(110) DEFAULT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `stock_manage`, `sub_products_id`, `brand`, `key_features`, `self_life`, `declaimer`, `created_at`, `updated_at`, `product_name`, `product_description`, `amount`, `quantity`, `product_image`) VALUES
(23, 'in_stock', 18, 'sudha', 'it is very helpful for us', 'sdcscs', 'XAXAXAXAS', '2020-10-09 10:51:35', '2020-10-09 10:51:35', 'cow milk', 'it is very helpful for us', 50.00, 1, '1602260495.jpg'),
(24, 'in_stock', 19, 'ddaasd', 'SAS', 'ASAAS', 'ASDASDASD', '2020-10-09 10:52:34', '2020-10-09 10:52:34', 'real juice', 'it is very helpful for us', 100.00, 1, '1602260554.jpg'),
(25, 'in_stock', 21, 'dell', 'sdfsfsf', 'sdfsf', 'sdfsdfs', '2020-10-09 10:53:28', '2020-10-09 10:53:28', 'Dell laptop', 'largest sell in india', 40000.00, 1, '1602260608.jpg'),
(26, 'in_stock', 21, 'hp', 'sdfsfsf', 'sdfsf', 'sdfsdfs', '2020-10-09 10:53:28', '2020-10-09 10:53:28', 'hp laptop', 'Hp largest sell in india', 45000.00, 1, '1602260608.jpg'),
(27, 'in_stock', 21, 'Apple', 'sdfsfsf', 'sdfsf', 'sdfsdfs', '2020-10-09 10:53:28', '2020-10-09 10:53:28', 'hp laptop', 'Apple largest sell in india', 100000.00, 1, '1602260608.jpg'),
(28, 'in_stock', 21, 'lenova', 'sdfsfsf', 'sdfsf', 'sdfsdfs', '2020-10-09 10:53:28', '2020-10-09 10:53:28', 'hp laptop', 'lenovalargest sell in india', 40000.00, 1, '1602260608.jpg'),
(29, 'in_stock', 19, 'ddaasd', 'SAS', 'ASAAS', 'ASDASDASD', '2020-10-09 10:52:34', '2020-10-09 10:52:34', 'pomegranate molasses.', 'it is very helpful for us', 150.00, 1, '1602260554.jpg'),
(30, 'in_stock', 19, 'ddaasd', 'SAS', 'ASAAS', 'ASDASDASD', '2020-10-09 10:52:34', '2020-10-09 10:52:34', 'Swasthi recipee', 'Swasthi recipee is very helpful for us', 130.00, 1, '1602260554.jpg'),
(31, 'in_stock', 19, 'ddaasd', 'SAS', 'ASAAS', 'ASDASDASD', '2020-10-09 10:52:34', '2020-10-09 10:52:34', 'Ministry of curry', 'Ministry of curryis very helpful for us', 200.00, 1, '1602260554.jpg'),
(32, 'in_stock', 18, 'panner item', 'it is very helpful for us', 'sdcscs', 'XAXAXAXAS', '2020-10-09 10:51:35', '2020-10-09 10:51:35', 'panner item', 'panner item is very helpful for us', 50.00, 1, '1602260495.jpg'),
(33, 'in_stock', 18, 'curditem', 'it is very helpful for us', 'sdcscs', 'XAXAXAXAS', '2020-10-09 10:51:35', '2020-10-09 10:51:35', 'curd item', 'curd item is very helpful for us', 60.00, 1, '1602260495.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sub_products`
--

CREATE TABLE `sub_products` (
  `id` int(110) NOT NULL,
  `main_products_id` int(110) NOT NULL,
  `product_name` text CHARACTER SET latin1,
  `product_description` text CHARACTER SET latin1,
  `product_image` text CHARACTER SET latin1,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_products`
--

INSERT INTO `sub_products` (`id`, `main_products_id`, `product_name`, `product_description`, `product_image`, `created_at`, `updated_at`) VALUES
(17, 17, 'Motherdairy product', 'Best milk in india', '1602260147.jpg', '2020-10-09 10:45:47', '2020-10-09 10:45:47'),
(18, 17, 'Amul milk', 'best milk in india', '1602260179.jpeg', '2020-10-09 10:46:19', '2020-10-09 10:46:19'),
(19, 18, 'Anar juice', 'it is very helpful for us', '1602260218.jpg', '2020-10-09 10:46:58', '2020-10-09 10:46:58'),
(20, 18, 'Apple Juice', 'it is very helpful for us. Apple juice', '1602260249.jpeg', '2020-10-09 10:47:29', '2020-10-09 10:47:29'),
(21, 19, 'computer accessories', 'largest sell in india', '1602260281.jpg', '2020-10-09 10:48:01', '2020-10-09 10:48:01'),
(22, 19, 'electronics item', 'best item in india', '1602260315.jpg', '2020-10-09 10:48:35', '2020-10-09 10:48:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Anik Roy', 'royanik28@gmail.com', '$2a$10$vtMQ/AM5gGPF8TLqHtn5oOnk0K7J4yjmT.BoSc96OOTUJyVJ/09Um', 'GqDccrSRdKXCyGvrwsyKBQIMdH8YFmj1tCgzOETIJQuvEnpkCwgCuoGvgrXJ', '2020-09-29 05:01:15', '2020-09-29 05:01:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `main_products`
--
ALTER TABLE `main_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `id_3` (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_products_id` (`sub_products_id`);

--
-- Indexes for table `sub_products`
--
ALTER TABLE `sub_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_products_id` (`main_products_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `main_products`
--
ALTER TABLE `main_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `sub_products`
--
ALTER TABLE `sub_products`
  MODIFY `id` int(110) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`sub_products_id`) REFERENCES `sub_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_products`
--
ALTER TABLE `sub_products`
  ADD CONSTRAINT `sub_products_ibfk_1` FOREIGN KEY (`main_products_id`) REFERENCES `main_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
