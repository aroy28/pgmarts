<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userwallets extends Model
{
    //
    protected $table = 'users_wallet';
	public $timestamps = false;
}
