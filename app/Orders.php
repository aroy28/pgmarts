<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users_address;
use App\Payments;
use App\Products;

class Orders extends Model
{
    //

    protected $table = 'orders';
    public $timestamps = false;
   
   public static function getOrderAddress($address_id){
          return Users_address::where('id', $address_id)->first();
   }

   public static function getOrderedProducts($pre_transaction_id){
          $paymentdata =  Payments::where('pre_transaction_id', $pre_transaction_id)->get();
          $paymentData=[];
          foreach($paymentdata as $key=>$val){
          	$arr =[];
          	$products = Products::where('id', $val->product_id)->first();
          	if(!empty($products)){
              $arr['product_name'] = $products->product_name;
              $arr['product_description'] = $products->product_description;
              $arr['product_image'] = $products->product_image;
          	}

            $arr['id'] = $val->id;
            $arr['product_id'] = $val->product_id;
            $arr['quantity'] = $val->quantity;
            $arr['product_price'] = $val->product_price;
            $arr['tax'] = $val->tax;
            $paymentData[] = $arr;
          }
          return $paymentData;
   }

}
