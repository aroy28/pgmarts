<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userpost extends Model
{
    //
    protected $table = 'users_post';
	public $timestamps = false;
}
