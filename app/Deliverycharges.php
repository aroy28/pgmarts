<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliverycharges extends Model
{
    protected $table = 'delivery_charges';
    public $timestamps = false;
}
