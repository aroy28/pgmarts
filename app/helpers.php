<?php 
use App\Orders;
use App\User;
use App\Status;

if(!function_exists('get_email')) {
   function get_email(){
   	 return 'test@gmail.com';
   }
}


if(!function_exists('send_order_status_email')) {
   function send_order_status_email($orderId) {
   	 $order_data = Orders::where('id', $orderId)->first();
     $userId = $order_data->user_id; 
     $usersdata = User::where('id', $userId)->first();
     $toemail = $usersdata->email;
     $toname = $usersdata->name;
     $order_data = $order_data->toarray();
     $status = Status::where('id', $order_data['status'])->first()->name;
     $orderedproducts= Orders::getOrderedProducts($order_data['pre_transaction_id']);
     $ordered_address_info= Orders::getOrderAddress($order_data['address_id']);
     if(!empty($orderedproducts)){
         $order_data['ordered_products'] = $orderedproducts;
     }

     if(!empty($ordered_address_info)){
         $order_data['ordered_address'] = $ordered_address_info;
     }
     $order_data['order_status'] = $status;
   	 Mail::send('order_status_mail', ['orders_data'=>$order_data], function($message) use ($toemail, $toname) {
         $message->to($toemail, $toname)->subject
            ('Order Status Mail');
         $message->from('no-reply@pgmarts.com','pgmarts');
      });
   }
}


if(!function_exists('send_updated_order_status_email')) {
   function send_updated_order_status_email($orderId) {
   	 $order_data = Orders::where('id', $orderId)->first();
     $userId = $order_data->user_id; 
     $usersdata = User::where('id', $userId)->first();
     $toemail = $usersdata->email;
     $toname = $usersdata->name;
     $order_data = $order_data->toarray();
     $status = Status::where('id', $order_data['status'])->first()->name;
     $orderedproducts= Orders::getOrderedProducts($order_data['pre_transaction_id']);
     $ordered_address_info= Orders::getOrderAddress($order_data['address_id']);
     if(!empty($orderedproducts)){
         $order_data['ordered_products'] = $orderedproducts;
     }

     if(!empty($ordered_address_info)){
         $order_data['ordered_address'] = $ordered_address_info;
     }
     $order_data['order_status'] = $status;
   	 Mail::send('updated_order_status_mail', ['orders_data'=>$order_data], function($message) use ($toemail, $toname) {
         $message->to($toemail, $toname)->subject
            ('Order Status Mail');
         $message->from('no-reply@pgmarts.com','pgmarts');
      });
   }
}



if(!function_exists('send_order_status_sms')) {
   function send_order_status_sms($orderId) {
	   	 $orders_data = Orders::where('id', $orderId)->first();
	     $userId = $orders_data->user_id; 
	     $usersdata = User::where('id', $userId)->first();
	     $toemail = $usersdata->email;
	     $toname = $usersdata->name;
	     $orders_data = $orders_data->toarray();
	     $status = Status::where('id', $orders_data['status'])->first()->name;
	   	
	   	// Authorisation details.
		$username = "anikroy@pgmarts.com";
		$hash = "230588765477dc57c900b6f1a26b6f90968449f58f7027ba04f87491bde4ce7a";

		// Config variables. Consult http://api.textlocal.in/docs for more info.
		$test = "0";

		// Data for text message. This is the text message data.
		$sender = "PGMARTS"; // This is who the message appears to be from.
		$phone = $usersdata->phone;
		$numbers = "91".$phone; // A single number or a comma-seperated list of numbers
		$delivery_date = $orders_data['delivery_date'];
		$grand_total = $orders_data['grand_total'];
		if($orders_data['status']==1){
            $message = "Thank you for shopping on Pgmart. We're prepping your order with 1 Parx Trousers. In these tough times, our team is working hard while ensuring highest safety standards, deliveries may take longer than usual. Your order $orderId is expected to be delivered by $delivery_date ";
		} else if ($orders_data['status']==3){
            $message = "Payment of Rs.$grand_total for Pgmart (order no: $orderId) via PhonePe has failed. If deducted, it will be refunded within 7 - 9 working days ";
		} else {
			$message = "Payment of Rs.$grand_total for Pgmart (order no: $orderId) via PhonePe has failed. If deducted, it will be refunded within 7 - 9 working days ";
		}
		
		// 612 chars or less
		// A single number or a comma-seperated list of numbers
		$message = urlencode($message);
		$data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
		$ch = curl_init('http://api.textlocal.in/send/?');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch); // This is the result from the API
		curl_close($ch);

   }
}




if(!function_exists('send_user_otp_sms')) {
   function send_user_otp_sms($otp, $phone) { 	
	   	// Authorisation details.
		$username =config('constants.SMS_USERNAME');

		$hash = config('constants.SMS_HASH');

		// Config variables. Consult http://api.textlocal.in/docs for more info.
		$test = "0";

		// Data for text message. This is the text message data.
		$sender = config('constants.SMS_SENDER'); // This is who the message appears to be from.
		$phone = $phone;
		$numbers = "91".$phone; // A single number or a comma-seperated list of numbers
   //$message = "Use $otp as your verification code on Pgmart. Team Pgmart ";
    $message = config('constants.SMS_TEMPLATE');
    $str_old = 'var_otp';
    $str_new ="$otp";
    $message = str_replace($str_old,$str_new,$message);		
		// 612 chars or less
		// A single number or a comma-seperated list of numbers
		$message = urlencode($message);
		$data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
		$ch = curl_init('http://api.textlocal.in/send/?');
    //echo $data; die;
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch); // This is the result from the API
		curl_close($ch);

   }
}



if(!function_exists('generate_razor_paysignature')) {
   function generate_razor_paysignature($order_id , $razorpay_payment_id) {
    $secret = config('constants.RAZOR_SECRET');
    $orderdata = $order_id.'|'.$razorpay_payment_id;
    $generated_signature = hash_hmac('sha256', $orderdata ,  $secret);
    return $generated_signature;

 }
}