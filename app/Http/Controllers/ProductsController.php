<?php
namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Sub_Products;
use App\Main_Products;

class ProductsController extends Controller
{
public function __construct()
{
$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$data['active_class']='product';
$products = Products::paginate(1);
$data['main_product']=Main_Products::All();
return view('home',$data) -> with('products',$products);
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
$data['active_class']='product';
$data['products_data']='create';
return view('products.create',$data);
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{

//ADD PHOTO
if (isset($request->product_image)){
$avatar= Input::file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if($size[0] > 1280 && $size[1] > 720) {//RESIZE PHOTO
Image::make($avatar)->resize(1280, 720)->save(public_path('/photo/' . $filename));
}
else{
Image::make($avatar)->save(public_path('/photo/' . $filename));
}
}
//ADD PRODUCT_NAME, PRODUCT_DESCRIPTION, AMOUNT AND QUANTITY
$product = new Products;
$product->product_name = $request->input('product_name');
$product->product_description = $request->input('product_description');
$product->sub_products_id = $request->input('sub_products_id');
$product->amount=$request->input('amount');
$product->quantity=$request->input('quantity');
$product->unit=$request->input('unit');
$product->stock_manage=$request->stock_manage;
$product->brand=$request->brand;
$product->key_features=$request->key_features;
$product->offer_rate=$request->input('offer_rate');
$product->declaimer=$request->declaimer;
$product->self_life=$request->self_life;
if (isset($request->product_image)){
$product->product_image = $filename;
}
$product->save();
return redirect('/home')->with('success');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show(Products $product)
{
$sub_product=Sub_Products::where('id',$product->sub_products_id)->first();
if (!empty($sub_product)) {
$data['subproduct']=$sub_product->product_name;
# code...
}
else
{
$data['subproduct']='--';
}
$mainproduct=Main_Products::where('id',$sub_product->main_products_id)->first();
if (!empty($mainproduct)) {
$data['mainproduct']=$mainproduct->product_name;
# code...
}
else
{
$data['mainproduct']='--';
}  
return view('products.show',$data) -> with('products',$product);
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit(Products  $product)
{
return view('products.edit')->with('product',$product);
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
//dd($request->all());
$products = Products::where('id',$id)->first();
$products->product_name = $request->product_name;
$products->product_description = $request->product_description;
$products->amount=$request->amount;
$products->quantity=$request->quantity;
$products->stock_manage=$request->stock_manage;
$products->sub_products_id = $request->sub_products_id;
$products->brand=$request->brand;
$products->key_features=$request->key_features;
$products->declaimer=$request->declaimer;
$products->self_life=$request->self_life;
$products->unit=$request->input('unit');
$products->offer_rate=$request->input('offer_rate');

//resize photo
if (isset($request->product_image)){
if($request->hasFile('product_image')){
$avatar = $request->file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if ($size[0]>1280 && $size[1]>720){
Image::make($avatar)->resize(1280,720)->save(public_path('photo/').$filename);
}
else{
Image::make($avatar)->save(public_path('photo/').$filename);
}
$oldFilename = $products->product_image;
//update the database
$products->product_image=$filename;
//Delete the old photo
Storage::delete($oldFilename);
}
}
$products->save();
return redirect('/home') -> with('edit','');
}
/**
* Remove the specified resource from storage.
*
* @param  Products $product
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
//delete image from local folder "/photo/"
$products = Products::where('id',$id)->take(1)->delete();
echo "success";
}
//FRONTEND CONTROL -  ANY PRODUCT SHOW LIST, AND  INDIVIDUAL PRODUCT VIEW
public function product_view(Products $product){
return view('products.product_view')->with('products',$product);
}
public function products_edit($id)
{
$data['active_class']='product';
$data['product']=Products::where('id',$id)->first();
$data['products_data']='edit';
return view('products.create',$data);
}
public function fetch_subproduct($id)
{
//dd($id);
$fetch_subproduct=Sub_Products::where('main_products_id',$id)->orderby('id','desc')->get();
//dd($cities);
$s='';
if(count($fetch_subproduct)){
$s.="
<option value='' >Select Sub Product</option>
";
foreach($fetch_subproduct as $key)
{
$s.="
<option value='$key->id'>$key->product_name</option>
";
}
}
else
{
$s.="
<option >No Sub Product Found</option>
";
}
return($s);
}
public function getproduct($id)
{
$data['products']=Products::where('sub_products_id',$id)->orderby('id','desc')->get();
return view('products.getproduct',$data);
}
}