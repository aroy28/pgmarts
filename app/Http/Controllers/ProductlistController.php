<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Main_Products;
use App\Sub_Products;
use App\Products;
use App\Deliverycharges;
use App\Units;

class ProductlistController extends Controller
{
    //

	 public function __construct()
	{
 //    	$this->middleware('auth');
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}


	public function main_products(Request $request){

		$main_products = Main_Products::orderby('id','desc')->get();

		$this->return['message'] = 'Main Products Listing.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $main_products;
		$status = 200;


       return response()->json($this->return, 200);


	}


	public function sub_products(Request $request){

		$validator = Validator::make($request->all(), [
		 'main_products_id' => 'required'
		]);

		 if ($validator->fails()) {
		        $this->return['status'] = 0;
		        $this->return['message'] = $validator->errors()->first();
		        $this->return['jsonData']['error'] = $validator->errors();
		        $status = 200;
		        return response()->json($this->return, $status);
		    }


			$id = $request->main_products_id;

			$sub_products = Sub_Products::where('main_products_id',$id)->orderby('id','desc')->get();

			$this->return['message'] = 'Sub Products Listing.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $sub_products;
			$status = 200;


		   return response()->json($this->return, 200);


	}



	public function products(Request $request){

		$validator = Validator::make($request->all(), [
		 'sub_products_id' => 'required'
		]);

		if ($validator->fails()) {
		        $this->return['status'] = 0;
		        $this->return['message'] = $validator->errors()->first();
		        $this->return['jsonData']['error'] = $validator->errors();
		        $status = 200;
		        return response()->json($this->return, $status);
		}
        $page    = $request->input('page') ?? 1;
        $per_page_size= 15;  
		$id = $request->sub_products_id;
        $input_data = $request->all();
        $in_stock = "in_stock"; 
		$products = products::where('stock_manage', 'LIKE', '%' . $in_stock . '%')->where('sub_products_id',$id);
		if (isset($input_data['search'])) {
			$search = $input_data['search'];
			$search = strtolower($search);
			$products = $products->where('product_name', 'LIKE', '%' . $search . '%');
		}
		$products=$products->orderby('id','desc')
					->offset(($page-1) * $per_page_size)
					->limit($per_page_size)
					->get();
        
        $product_arr =[];
		foreach($products as $prodct){
			$array =[];
			 $sub_products= Sub_Products::where('id',$id)->first();
			if(!empty($sub_products)){
				$prodct->sub_product_name = $sub_products->product_name;
			}

           $product_arr[] = $prodct;
		}

		$this->return['message'] = 'Products Listing.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $product_arr;
		$this->return['page'] = $page;
		$status = 200;


		return response()->json($this->return, 200);


	}



	public function products_list(Request $request){

		$page = $request->input('page') ?? 1;
		$per_page_size= 15;  
		$in_stock = "in_stock";
		$products = products::where('stock_manage', 'LIKE', '%' . $in_stock . '%')->orderby('id','desc');
		$input_data = $request->all();
		if (isset($input_data['search'])) {
			$search = $input_data['search'];
            $search = strtolower($search);
            $products = $products->where('product_name', 'LIKE', '%' . $search . '%');
        }
		$products = $products->offset(($page-1) * $per_page_size)
		->limit($per_page_size)
		->get();

        $product_arr =[];
		foreach($products as $prodct){
			$array =[];
			 $sub_products= Sub_Products::where('id',$prodct->sub_products_id)->first();
			if(!empty($sub_products)){
				$prodct->sub_product_name = $sub_products->product_name;

				$main_products= Main_Products::where('id',$sub_products->main_products_id)->first();
				if(!empty($main_products)){
					$prodct->main_product_name = $main_products->product_name;
				}else{
					$prodct->main_product_name ="";
				}


			}else{
				$prodct->sub_product_name = "";
				$prodct->main_product_name ="";
			}


           $product_arr[] = $prodct;
		}


		$this->return['message'] = 'Products Listing.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $product_arr;
		$this->return['page'] = $page;
		$status = 200;


		return response()->json($this->return, 200);


	}


	public function delivery_charges(Request $request){
        $validator = Validator::make($request->all(), [
		 'amount' => 'required'
		]);

		if ($validator->fails()) {
		        $this->return['status'] = 0;
		        $this->return['message'] = $validator->errors()->first();
		        $this->return['jsonData']['error'] = $validator->errors();
		        $status = 200;
		        return response()->json($this->return, $status);
		}

		$amount = $request->amount;
		$deliveryCharges = Deliverycharges::whereRaw("$amount BETWEEN start_amount AND end_amount")
		 ->first();

		$this->return['message'] = 'Delivery Charges in Rs.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $deliveryCharges;
		$status = 200;
		return response()->json($this->return, 200);


	}


	public function get_units(Request $request){
       
		$getUnits = Units::all();
		$this->return['message'] = 'All Units.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $getUnits;
		$status = 200;
		return response()->json($this->return, 200);


	}


}
