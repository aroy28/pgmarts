<?php
namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;
class HomeController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
/**
* Show the application dashboard.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$products = Products::paginate(5);
$data['active_class']='product';
return view('home',$data)->with('products',$products);
}
public function product_show(){
$products = Products::paginate(5);
return view('products.product_show')->with('products',$products);
}
}