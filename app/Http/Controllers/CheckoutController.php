<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Payments;
use App\Invoice;
use App\Orders;
use App\Userwallets;
use App\Coupon;
use App\Status;
use App\OrderStatusDetails;
use App\Cart;

class CheckoutController extends Controller
{
    public function __construct()
	{
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}

  //initialize payment 
	public function initialize_payment_checkout(Request $request){

			$validator = Validator::make($request->all(), [
			     'user_id' => 'required', 'order_data' => 'required','grand_total' => 'required','pay_via' => 'required','address_id' => 'required', 'delivery_charge' => 'required'  
			]);

			if ($validator->fails()) {
	            $this->return['status'] = 0;
	            $this->return['message'] = $validator->errors()->first();
	            $this->return['jsonData']['error'] = $validator->errors();
	            $status = 200;
	            return response()->json($this->return, $status);
	        }
            
            if(isset($request->order_data) &&!empty($request->order_data)){
            	 foreach($request->order_data as $dat){
            	 	 if($dat['product_id'] =='' || $dat['product_price'] =='' || !isset($dat['quantity']) || $dat['quantity'] ==''){
						$this->return['message'] = 'Please check product id,product price, quantity in Order data';
						$this->return['status'] = 0;
						$this->return['jsonData']['data'] = $request->all();
						return response()->json($this->return, 200);  die;
            	 	 }

            	 }
            }

	        $user_id = $request->user_id;
	        $grand_total = $request->grand_total;
	        $pay_via = $request->pay_via;
	        $data = $request->all();
			$maxid = Orders::select('id')->orderBy('id', 'desc')->first();
			if(empty($maxid)){
			  $key = 'PGMART000001';
			}else{
			   $key = 'PGMART00000' . ++$maxid->id;
			}

			//check if wallet
			if(strtolower($pay_via)=='wallet'){
              $Userwallets = Userwallets::where('user_id', $user_id);
              $Userwallets = $Userwallets->get()->toArray();
              $wallet_balance=0;
	            foreach($Userwallets as $walet){
	             $wallet_balance= $wallet_balance + $walet['amount'];
	            }

	           if($grand_total > $wallet_balance){
					$this->return['message'] = 'Insufficient wallet amount';
					$this->return['status'] = 0;
					$this->return['jsonData']['data'] = $request->all();
					return response()->json($this->return, 200);  die;
	           } 
			}

			//end of check if wallet

			date_default_timezone_set('Asia/Kolkata');
			$date_time = Date('Y-m-d h:i:s');
			$coupon_id="";
			if(isset($request->applied_coupon) && !empty($request->applied_coupon)){
				$coupon_code = $request->applied_coupon;
				$check_coupon = Coupon::where('coupon_code', $coupon_code)->first();
				if($check_coupon){
					$coupon_id = $check_coupon->id;
				}
			}

	        foreach($data['order_data'] as $val){
		        $array = array(
		            'user_id' => $user_id,
		            'product_id' => $val['product_id'],
		            'quantity'  => $val['quantity'],
		            'pre_transaction_id' => $key,
		            'product_price' => $val['product_price'],
		            'applied_coupon' => $request->applied_coupon,
		            'coupon_id' => $coupon_id,
		            'order_date' => $date_time
		       );

		       Payments::insert($array);
	        }
            
            $this->return['message'] = 'Payment initialized.';
	        $order_status = config('constants.status.pending');
	        $check_cod = strtolower($request->pay_via);
	        if ((strpos($check_cod, 'cod') !== false) || (strpos($check_cod, 'COD') !== false )  || (strpos($check_cod, 'cash on delivery') !== false )  || (strpos($check_cod, 'cash') !== false )) {
                $order_status = config('constants.status.ordered');
                $this->return['message'] = 'Payment will be on the Cash on delivery.';
	        }

	        $array = array(
	            'user_id' => $request->user_id,
	            'pre_transaction_id' => $key,
	            'tax' => $request->tax,
	            'grand_total' => $request->grand_total,
	            'applied_coupon' => $request->applied_coupon,
	            'coupon_id' => $coupon_id,
	            'order_date' => $date_time,
	            'status'=>$order_status,
	            'payment_mode' => $request->payment_mode,
	            'pay_via'=>$request->pay_via,
	            'unit'=>$request->unit,
	            'address_id'=>$request->address_id,
	            'delivery_charge'=>$request->delivery_charge
           );

	       $orderData = Orders::insert($array);
	       $Orders = Orders::where('user_id', $request->user_id)->where('pre_transaction_id', $key)->first();
			$orderStatusdata =[
				'order_id'=>$Orders->id,
				'user_id'=>$request->user_id,
				'status_id'=> $order_status
			];
			OrderStatusDetails::saveOrderStatus($orderStatusdata);

	        if(strtolower($pay_via)=='wallet'){
               $this->update_order_in_wallet_case($request, $key);
	        }

			
			$this->return['status'] = 1;
			$this->return['jsonData']['data'] = $request->all();
			$this->return['jsonData']['data']['order_id'] = $key;
			return response()->json($this->return, 200);


	}


	//final payment
	public function final_payment_checkout(Request $request){
        
        $validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'order_id' => 'required',  
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }
        
        $allinput = $request->all();
        $pre_transaction_id = $request->order_id;
        $user_id = $request->user_id;
        $Orders = Orders::where('user_id', $user_id)->where('pre_transaction_id', $pre_transaction_id)->first();
        if(empty($Orders)){
        	$this->return['message'] = 'Invalid Data';
			$this->return['status'] = 0;
			$status = 200;
			return response()->json($this->return, 200);  die;
        }
        
        // handle razor payment signature
        $razorpay_payment_id='';
        $razorpay_order_id='';
        $razorpay_signature='';
        if(isset($allinput['razorpay_payment_id'])){
           $razorpay_payment_id = $allinput['razorpay_payment_id'];
        }
        if(isset($allinput['razorpay_order_id'])){
          $razorpay_order_id = $allinput['razorpay_order_id'];
        }
        if(isset($allinput['razorpay_signature'])){
          $razorpay_signature = $allinput['razorpay_signature'];
        }

        

		$Orders->razorpay_payment_id = $razorpay_payment_id;
		$Orders->razorpay_order_id = $razorpay_order_id;
		$Orders->razorpay_signature = $razorpay_signature;
        
        $order_id = $pre_transaction_id;
		$generated_signature = generate_razor_paysignature($order_id , $razorpay_payment_id);
		if ($generated_signature == $razorpay_signature) {
		      //payment is successful
             $Orders->status =config('constants.status.ordered');
             $orderstatus = config('constants.status.ordered');
		} else {
            $Orders->status =config('constants.status.failed');
            $orderstatus = config('constants.status.failed');
		}

		//end of handle razor payment signature

		//cod payment
		$check_cod = $Orders->pay_via;
		if ((strpos($check_cod, 'cod') !== false) || (strpos($check_cod, 'COD') !== false )  || (strpos($check_cod, 'cash on delivery') !== false )  || (strpos($check_cod, 'cash') !== false )) {
            $orderstatus = config('constants.status.ordered');
            $Orders->status =config('constants.status.ordered');
	    }

        date_default_timezone_set('Asia/Kolkata');
        $now = Date('Y-m-d H:i:s');
        $hours = 24;
        $delevery_date_time = date('Y-m-d H:i:s',strtotime("+$hours hour",strtotime($now)));
        $Orders->delivery_date = $delevery_date_time;
        $Orders->save();

		$orderStatusdata =[
			'order_id'=>$Orders->id,
			'user_id'=>$user_id,
			'status_id'=>$orderstatus
		];
		OrderStatusDetails::saveOrderStatus($orderStatusdata);

        $trnsaction_id = $Orders->id;
        $checkInvoice = Invoice::where('transaction_id', $trnsaction_id)->first();
        if(!empty($checkInvoice)){
			$this->return['message'] = 'This transaction already done';
			$this->return['status'] = 0;
			$status = 200;
			return response()->json($this->return, 200);  die;
        }

        $invoice= new Invoice;
        $invoice->invoice_id = 0;
        $invoice->transaction_id= $trnsaction_id;
        $invoice->save();
        $last_id = $invoice->id;
        Invoice::where('id', $last_id)->update(array('invoice_id'=>$last_id));
        //remove data from cart 
        Cart::where('user_id', $user_id)->delete();

		$this->return['message'] = 'Payment complete please check order status.';
		$this->return['status'] = 1;
		$this->return['jsonData']['data'] = $request->all();
		return response()->json($this->return, 200);


	}


	private function update_order_in_wallet_case($request, $pre_transaction_id){
        $pre_transaction_id = $pre_transaction_id;
        $user_id = $request->user_id;
        $Orders = Orders::where('user_id', $user_id)->where('pre_transaction_id', $pre_transaction_id)->first();
        $Orders->status =1;
        date_default_timezone_set('Asia/Kolkata');
        $now = Date('Y-m-d H:i:s');
        $hours = 24;
        $delevery_date_time = date('Y-m-d H:i:s',strtotime("+$hours hour",strtotime($now)));
        $Orders->delivery_date = $delevery_date_time;
        $Orders->save();

        $trnsaction_id = $Orders->id;
        $checkInvoice = Invoice::where('transaction_id', $trnsaction_id)->first();
        if(!empty($checkInvoice)){
			$this->return['message'] = 'This transaction already done';
			$this->return['status'] = 0;
			$status = 200;
			return response()->json($this->return, 200);  die;
        }

        $invoice= new Invoice;
        $invoice->invoice_id = 0;
        $invoice->transaction_id= $trnsaction_id;
        $invoice->save();
        $last_id = $invoice->id;
        Invoice::where('id', $last_id)->update(array('invoice_id'=>$last_id));

        $wallet = new Userwallets;
        $wallet->user_id = $request->user_id;
        $wallet->amount = "-".$request->grand_total;
        $wallet->description = "Product purchased by using wallet amount";
        $wallet->save();

	}
     

    public function get_order_status_list(Request $request){
        
        $data = Status::all(); 
		$this->return['message'] = 'Status list fetched sucessfully';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $data;
		$status = 200;
		return response()->json($this->return, 200); die;

	} 



}
