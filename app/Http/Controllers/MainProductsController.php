<?php
namespace App\Http\Controllers;
use App\Main_Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
class MainProductsController extends Controller
{
public function __construct()
{
$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$products = Main_Products::orderby('id','desc')->paginate(5);
$data['active_class']='mainproduct';
return view('main_product.list',$data)->with('products',$products);
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
$data['products_data']='create';
$data['active_class']='mainproduct';
return view('main_product.create',$data);
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//dd($request->all());
$this->validate($request, [
'product_name' => 'required|min:3|max:50',
'product_description' => 'required|min:10',
'product_image'=>'image'
]);
//ADD PHOTO
if (isset($request->product_image)){
$avatar= Input::file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if($size[0] > 1280 && $size[1] > 720) {//RESIZE PHOTO
Image::make($avatar)->resize(1280, 720)->save(public_path('/photo/' . $filename));
}
else{
Image::make($avatar)->save(public_path('/photo/' . $filename));
}
}
//ADD PRODUCT_NAME, PRODUCT_DESCRIPTION, AMOUNT AND QUANTITY
$product = new Main_Products;
$product->product_name = $request->input('product_name');
$product->product_description = $request->input('product_description');
if (isset($request->product_image)){
$product->product_image = $filename;
}
$product->save();
return redirect('/mainproducts')->with('success');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show(Products $product)
{
return view('main_product.show') -> with('products',$product);
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit(Products  $product)
{
return view('main_product.edit')->with('product',$product);
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
$products = Main_Products::find($id);
$products->product_name = $request->product_name;
$products->product_description = $request->product_description;
//resize photo
if (isset($request->product_image)){
if($request->hasFile('product_image')){
$avatar = $request->file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if ($size[0]>1280 && $size[1]>720){
Image::make($avatar)->resize(1280,720)->save(public_path('photo/').$filename);
}
else{
Image::make($avatar)->save(public_path('photo/').$filename);
}
$oldFilename = $products->product_image;
//update the database
$products->product_image=$filename;
//Delete the old photo
Storage::delete($oldFilename);
}
}
$products->save();
return redirect('/mainproducts') -> with('edit','');
}
/**
* Remove the specified resource from storage.
*
* @param  Products $product
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
	//dd($id);
$products = Main_Products::where('id',$id)->take(1)->delete();
echo "success";
}
//FRONTEND CONTROL -  ANY PRODUCT SHOW LIST, AND  INDIVIDUAL PRODUCT VIEW
public function product_show(){
$products = Main_Products::paginate(5);
return view('main_product.product_show')->with('products',$products);
}
public function product_view(Products $product){
return view('main_product.product_view')->with('products',$product);
}
public function products_edit($id)
{
$data['product']=Main_Products::where('id',$id)->first();
$data['products_data']='edit';
$data['active_class']='mainproduct';
return view('main_product.create',$data);
}
}