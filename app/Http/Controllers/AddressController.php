<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users_address;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\User;

class AddressController extends Controller
{
    //

     public function __construct()
	{
 //    	$this->middleware('auth');
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}


	public function add_address(Request $request){

		$validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'name' => 'required','area_locality' => 'required', 'house_no' => 'required', 'address_info' => 'required','pincode' => 'required', 'address_info_type' => 'required', 
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

		$user_add = new Users_address;
		$user_add->user_id = $request->user_id;
		$user_add->name = $request->name;
		$user_add->area_locality = $request->area_locality;
		$user_add->house_no = $request->house_no;
		$user_add->address_info = $request->address_info;
		$user_add->pincode = $request->pincode;
		$user_add->address_info_type = $request->address_info_type;
		$user_add->phone = $request->phone;
		$user_add->save();

		$this->return['message'] = 'Address Added Successfully.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;

       return response()->json($this->return, 200);



    }

    public function edit_address(Request $request){

		$validator = Validator::make($request->all(), [
		     'user_id' => 'required','address_id' => 'required', 'name' => 'required','area_locality' => 'required', 'house_no' => 'required', 'address_info' => 'required','pincode' => 'required', 'address_info_type' => 'required', 
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }
        $inputs = $request->all();

		$user_add = Users_address::where('id', $inputs['address_id'])->first();
		$user_add->user_id = $request->user_id;
		$user_add->name = $request->name;
		$user_add->area_locality = $request->area_locality;
		$user_add->house_no = $request->house_no;
		$user_add->address_info = $request->address_info;
		$user_add->pincode = $request->pincode;
		$user_add->address_info_type = $request->address_info_type;
		$user_add->phone = $request->phone;
		$user_add->save();

		$this->return['message'] = 'Address edited Successfully.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;

       return response()->json($this->return, 200);



    }


    public function get_my_address(Request $request){

			$validator = Validator::make($request->all(), [
			 'user_id' => 'required',
			]);

			if ($validator->fails()) {
			    $this->return['status'] = 0;
			    $this->return['message'] = $validator->errors()->first();
			    $this->return['jsonData']['error'] = $validator->errors();
			    $status = 200;
			    return response()->json($this->return, $status);
			} 

			$user_id = $request->user_id;
            
            $address = Users_address::where('user_id', $user_id);
            $Users_address = $address->get()->toArray();

			$this->return['message'] = 'Address list.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $Users_address;
			$status = 200;

			return response()->json($this->return, 200);	



    }

    public function delete_my_address(Request $request){

			$validator = Validator::make($request->all(), [
			 'user_id' => 'required','address_id' => 'required',
			]);

			if ($validator->fails()) {
			    $this->return['status'] = 0;
			    $this->return['message'] = $validator->errors()->first();
			    $this->return['jsonData']['error'] = $validator->errors();
			    $status = 200;
			    return response()->json($this->return, $status);
			} 

			$user_id = $request->user_id;
			$address_id = $request->address_id;
            
            $address = Users_address::where('user_id', $user_id)->where('id', $address_id)->delete();
           
            $addressinfo = Users_address::where('user_id', $user_id);
            $Users_addressinfo = $addressinfo->get()->toArray();

			$this->return['message'] = 'Address deleted sucessfully.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $Users_addressinfo;
			$status = 200;

			return response()->json($this->return, 200);	



    }


    //update password 
    public function update_password(Request $request){
         
				$validator = Validator::make($request->all(), [
				  'user_id' => 'required','current_password' => 'required', 'confirm_new_password' => 'required|same:new_password','new_password' => 'required', 
				]);

	         if ($validator->fails()) {
	            $this->return['status'] = 0;
	            $this->return['message'] = $validator->errors()->first();
	            $this->return['jsonData']['error'] = $validator->errors();
	            $status = 200;
	            return response()->json($this->return, $status);
	        }

	        $user_id = $request->user_id;
	        $request_data = $request->all();

	        $check_user = User::where('id', $user_id)->first();
	        if(empty($check_user)){
	            $this->return['message'] = 'User not found';
				$this->return['status'] = 0;
				$status = 200;
				return response()->json($this->return, 200);  die;
	        }

	        $current_password = $check_user->password;           
	      if(Hash::check($request_data['current_password'], $current_password))
	      {           
	        $check_user->password = Hash::make($request_data['confirm_new_password']);
	        $check_user->save(); 
			$this->return['message'] = 'Password Changed successfully';
			$this->return['status'] = 1;
			$status = 200;
	      }
	      else{

				$this->return['message'] = 'Current password did not matched';
				$this->return['status'] = 0;
				$status = 401;
		  }

	       return response()->json($this->return, 200);  		  


    }


    public function get_address_by_id(Request $request){
	    $validator = Validator::make($request->all(), [
			 'user_id' => 'required','address_id' => 'required',
			]);

		if ($validator->fails()) {
		    $this->return['status'] = 0;
		    $this->return['message'] = $validator->errors()->first();
		    $this->return['jsonData']['error'] = $validator->errors();
		    $status = 200;
		    return response()->json($this->return, $status);
		}

		$user_id = $request->user_id;
		$address_id = $request->address_id;
		$address = Users_address::where('user_id', $user_id)->where('id', $address_id)->first(); 
		$this->return['message'] = 'Address not found';
		$this->return['status'] = 0;
		if($address){
			$this->return['message'] = 'Address fetched sucessfully';
			$this->return['status'] = 1;
		}
		
		$this->return['jsonData'] = $address;
		$status = 200;
		return response()->json($this->return, 200); die;

    }




}
