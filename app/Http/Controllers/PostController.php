<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userpost;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class PostController extends Controller
{
    //

		public function __construct()
		{
		//    	$this->middleware('auth');
		 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
		}


	  public function add_post(Request $request){

		$validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'title' => 'required','description' => 'required',  'image' => 'required',
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

		$user_add = new Userpost;
		$user_add->user_id = $request->user_id;
		$user_add->title = $request->title;
		$user_add->description = $request->description;
		$user_add->image = $request->image;
		$user_add->save();

		$this->return['message'] = 'Post Added Successfully.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;

       return response()->json($this->return, 200);



    }	




}
