<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Coupon;
use App\Orders;

class CouponController extends Controller
{
    //
     public function __construct()
	{
 //    	$this->middleware('auth');
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}

     

   public function apply_coupon(Request $request){ 
        
        $validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'coupon_code' => 'required',  
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

		$this->return['message'] = 'Invalid Coupon.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;
         //die('--');
          $coupon_code = $request->coupon_code;
          $check_coupon = Coupon::where('coupon_code', $coupon_code)->first();
          //print_r($check_coupon);  die('--');
          if(empty($check_coupon)){

			$this->return['message'] = 'Coupon does not exists.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $request->all();
			$status = 200;

			return response()->json($this->return, 200);

          }else{ 
	          	$user_id = $request->user_id;
	          	$users_ids = $check_coupon->users_id;

	     //      	if($users_ids!='' && $check_coupon->coupon_type='1' ){ 
	     //      		 $users_arr = explode(',', $users_ids);
	     //      		 if(in_array($user_id , $users_arr)){ 

						// date_default_timezone_set('Asia/Kolkata');
						// $current_date= Date('Y-m-d h:i:s');
						// $current_date_1 = date("Y-m-d", strtotime($current_date));

						// $start_date = $check_coupon->coupon_start_date;
						// $end_date = $check_coupon->coupon_end_date;
						// $start_date_1=  date("Y-m-d", strtotime($start_date));
						// $end_date_1=  date("Y-m-d", strtotime($end_date)); 

						// $start_date_1_time = strtotime($start_date_1);
						// $now = strtotime($current_date_1);
						// $datediff_withstart = $now - $start_date_1_time;
						// $end_date_1_time = strtotime($end_date_1);
						// $datediff_withend = $now - $end_date_1_time;

						// $check_start_date= round($datediff_withstart / (60 * 60 * 24));
						// $check_end_date = round($datediff_withend / (60 * 60 * 24));
                         
      //                    if($check_start_date >=0 && $check_end_date <=0 ){
						// 	$this->return['message'] = 'Coupon Valid.';
						// 	$this->return['status'] = 1;
						// 	$this->return['jsonData'] = $check_coupon;
						// 	$status = 200;
						// 	return response()->json($this->return, 200); die;
      //                    }else{
						// 	$this->return['message'] = 'Invalid Coupon.';
						// 	$this->return['status'] = 2;
						// 	$status = 200;
						// 	return response()->json($this->return, 200); die;
      //                    }

	     //      		 }else{
						// $this->return['message'] = 'Invalid Coupon.';
						// $this->return['status'] = 1;
						// $this->return['jsonData'] = $request->all();
						// $status = 200;
						// return response()->json($this->return, 200);

	     //      		 }

	     //      	}else{  

						date_default_timezone_set('Asia/Kolkata');
						$current_date= Date('Y-m-d h:i:s');
						$current_date_1 = date("Y-m-d", strtotime($current_date));

						$start_date = $check_coupon->coupon_start_date;
						$end_date = $check_coupon->coupon_end_date;
						$start_date_1=  date("Y-m-d", strtotime($start_date));
						$end_date_1=  date("Y-m-d", strtotime($end_date));

						$start_date_1_time = strtotime($start_date_1);
						$now = strtotime($current_date_1);
						$datediff_withstart = $now - $start_date_1_time;
						$end_date_1_time = strtotime($end_date_1);
						$datediff_withend = $now - $end_date_1_time;

						$check_start_date= round($datediff_withstart / (60 * 60 * 24));
						$check_end_date = round($datediff_withend / (60 * 60 * 24));
						$check_orders_of_user = Orders::where('status', '1')->where('user_id', $request->user_id)->first();
                         
                         if($check_start_date >=0 && $check_end_date <=0 && empty($check_orders_of_user)){
							$this->return['message'] = 'Coupon Valid.';
							$this->return['status'] = 1;
							$this->return['jsonData'] = $check_coupon;
							$status = 200;
							return response()->json($this->return, 200); die;
                         }else{
							$this->return['message'] = 'Invalid Coupon.';
							$this->return['status'] = 2;
							$status = 200;
							return response()->json($this->return, 200); die;
                         }


	          //	}


          }

          return response()->json($this->return, 200);



   }



}
