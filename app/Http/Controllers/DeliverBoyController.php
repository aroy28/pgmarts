<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\OrderStatusDetails;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Status;

class DeliverBoyController extends Controller
{
    //
    
	public function __construct()
	{
	 $this->middleware('auth');
	}

    public function getOrders(Request $request){

		$order = Orders::select('orders.*', 'status.name as order_status' )->join('status', 'status.id','orders.status');

		if(!empty($request->status)){
		 $order = $order->where('status', $request->status);
		}

		if(!empty($request->user_id)){
		 $order = $order->where('user_id', $request->user_id);
		}

		$order = $order->orderby("id","DESC");
		$orderlist = $order->paginate(5);
		//print_r($order_summary);
       $statuses = Status::get(); 
       return view('delivery_boy/orders', ['orders'=>$orderlist, 'active_class'=>'order-list', 'statuses'=>$statuses]);

    }

    //get order details by id
    public function getOrderDetails($id){
    	$orderDetails = Orders::select('orders.*', 'status.name as order_status' )->join('status', 'status.id','orders.status')->where('orders.id', $id);

		$orderDetails = $orderDetails->get();
		$orderDetailed = $orderDetails->toarray();
		$orderedproducts = [];
		if (!empty($orderDetails)) {
          $orderedproducts= Orders::getOrderedProducts($orderDetailed['0']['pre_transaction_id']);
		}
		//print_r($orderedproducts);  die;
		$statuses = Status::get();

       return view('delivery_boy/order_details', ['orders'=>$orderDetails, 'active_class'=>'order-list', 'orderedproducts'=>$orderedproducts, 'statuses'=>$statuses]);

    }

    //change_order_status by order id
    public function change_order_status(Request $request){
		$orders_data = Orders::where('id', $request->orderid)->first();
		if($orders_data){
			$data =[
				'order_id'=>$request->orderid,
				'user_id'=>$orders_data->user_id,
				'status_id'=>$request->status,
				'order_remarks'=>$request->order_remarks
		    ];
			OrderStatusDetails::saveOrderStatus($data);
			$orders_data->status = $request->status;
			$orders_data->order_remarks = $request->order_remarks;
			$orders_data->save();
			send_updated_order_status_email($request->orderid);
			$this->return['message'] = 'Order updated sucessfully';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $data;
		}else{
			$this->return['message'] = 'Order not found';
			$this->return['status'] = 0;
		}
		return response()->json($this->return, 200);

    }


}
