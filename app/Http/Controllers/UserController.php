<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
	{
	 $this->middleware('auth');
	}


	public function addUser(Request $request) {
        $roles = config('constants.role');
		return view('users/add', ['active_class'=>'user-list','roles'=>$roles]);

	}

	public function postAddUser(Request $request) {
		$this->validate($request, [
			'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
            'phone' => 'required|string',
            'role' => 'required',
        ]);
        
        $email = $request->email;
        $check_user = User::where('email', $email)->first();
        $phone = $request->phone;
        $check_phone = User::where('phone', $phone)->first();
        if(empty($check_user) && empty($check_phone) ){            
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            $user->role = $request->role;
            $user->status = '1';
            $user->save();
            return redirect()->intended('/user_list')->with('addusersucessmsg', "User Added!");
		  }else{
             return redirect()->intended('/add_user')->withInput($request->input())->with('addusererrormsg', "Phone or Email Already Exists!");
		  }
        
	}


	public function user_list(Request $request) {
        $roles = config('constants.role');
        $name = '';
        $email = '';
        $searchrole = '';
        $status ='1';
        if(isset($request->status)){
            $status = $request->status;
            $usersdata = User::where('status', $status);
        } else {
           $usersdata = User::where('status' , '1');
        }

         if(isset($request->status)){
           $status = $request->status;
           $usersdata = $usersdata->where('status', $status);
         }

         if(isset($request->name)){
           $name = $request->name;
           $usersdata = $usersdata->where('name', 'LIKE', '%'.$name.'%');
         }

         if(isset($request->email)){
           $email = $request->email;
           $usersdata = $usersdata->where('email', 'LIKE', '%'.$email.'%');
         }

         if(isset($request->role)){
           $searchrole = $request->role;
           $usersdata = $usersdata->where('role', $searchrole);
         }


         $usersdata = $usersdata->paginate(5);
		return view('users/list', ['active_class'=>'user-list','roles'=>$roles,'name'=>$name, 'email'=>$email, 'email'=>$email,'searchrole'=>$searchrole,'status'=>$status, 'users'=>$usersdata]);
	}

	public function delete_user($id){
		$usersdata = User::where('id', $id)->first();
		$usersdata->status = '2';//2 for deleted
		$usersdata->save();
        return redirect()->back()->with('deletemsg', 'User record deleted successfully');
	}

   public function restore_user($id){
        $usersdata = User::where('id', $id)->first();
        $usersdata->status = '1';//1 for restore_user
        $usersdata->save();
        return redirect()->back()->with('deletemsg', 'User record stored successfully');
    }

    public function edit_user($id){
        $usersdata = User::where('id', $id)->first();
        $roles = config('constants.role');
        return view('users/edit', ['active_class'=>'user-list','roles'=>$roles, 'usersdata'=>$usersdata]);
    }

    public function update_user(Request $request){
        $this->validate($request, [
            'name' => 'required|string',
            'user_id' => 'required|string',
            'email' => 'required|string',
            //'password' => 'string|min:6|max:20',
            'phone' => 'required|string',
            'role' => 'required',
        ]);
        $usersdata = User::where('id', $request->user_id)->first();

        $check_user = User::where('email', $request->email)->where('id', '!=', $request->user_id)->first();
        $phone = $request->phone;
        $check_phone = User::where('phone', $request->phone)->where('id', '!=', $request->user_id)->first();
        if(empty($check_user) && empty($check_phone) ){ 
            $usersdata->name = $request->name;
            $usersdata->email = $request->email;
            $usersdata->phone = $request->phone;
            if(isset($request->password) && $request->password!=''){
               $usersdata->password = Hash::make($request->password); 
            }
            
            $usersdata->role = $request->role;
            $usersdata->status = '1';
            $usersdata->save();
            return redirect()->intended('/user_list')->with('addusersucessmsg', "User Updated!");
          }else{
             return redirect()->intended('/add_user')->withInput($request->input())->with('addusererrormsg', "Phone or Email Already Exists!");
          }
    }




}
