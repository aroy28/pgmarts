<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use DB;
use App\CreatorJwt;

class LoginController extends Controller
{
    //
  
  public $jwtobj;

	public function __construct() {
	    $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
      $this->jwtobj = new CreatorJwt();
	}
  

  public function send_otp(Request $request){

	$validator = Validator::make($request->all(), [
	     'name' => 'required','email' => 'required', 'phone' => 'required|digits:10|numeric', 'password' => 'required', 
	]);

	 if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }
      
      $email = $request->email;
      $phone = $request->phone;

      $check_user = User::where('email', $email)->first();
      $check_phone = User::where('phone', $phone)->first();
      if(empty($check_user) && empty($check_phone) ){
         
	        $otp = rand(1000, 9999);
	        $request->otp= $otp;
            
            DB::table('manage_otp')->where('phone', $request->phone)->delete();
	          DB::table('manage_otp')->insert(array('phone'=>$request->phone , 'otp'=>$otp, 'email'=>$request->email));
          send_user_otp_sms($otp, $phone);
      		$this->return['message'] = 'OTP Sent Successfully.';
      		$this->return['status'] = 1;
      		$this->return['jsonData'] = $request->all();
      		$this->return['jsonData']['otp'] = $otp;
      		$status = 200;

      }else{
        	$this->return['message'] = 'phone or email Already Exists.';
        	$this->return['status'] = 0;
        	$status = 401;
           
      }

     return response()->json($this->return, 200);

  }

 
 //resend otp 
  public function resend_otp(Request $request){

        $validator = Validator::make($request->all(), [
          'phone' => 'required|digits:10|numeric'
      ]);

       if ($validator->fails()) {
                $this->return['status'] = 0;
                $this->return['message'] = $validator->errors()->first();
                $this->return['jsonData']['error'] = $validator->errors();
                $status = 200;
                return response()->json($this->return, $status);
        }

          $otp = rand(1000, 9999);
          $request->otp= $otp;
          $phone = $request->phone;
            
            DB::table('manage_otp')->where('phone', $request->phone)->delete();
            DB::table('manage_otp')->insert(array('phone'=>$request->phone , 'otp'=>$otp, 'email'=>$request->email));
          send_user_otp_sms($otp, $phone); 
          $this->return['message'] = 'OTP Sent Successfully.';
          $this->return['status'] = 1;
          $this->return['jsonData'] = $request->all();
          $this->return['jsonData']['otp'] = $otp;
          $status = 200;

         return response()->json($this->return, 200);  


  }


  public function register(Request $request){

		$validator = Validator::make($request->all(), [
		     'name' => 'required', 'email' => 'required', 'phone' => 'required|digits:10|numeric', 'password' => 'required', 'otp' => 'required', 
		]);

		 if ($validator->fails()) {
		        $this->return['status'] = 0;
		        $this->return['message'] = $validator->errors()->first();
		        $this->return['jsonData']['error'] = $validator->errors();
		        $status = 200;
		        return response()->json($this->return, $status);
		    }
        
          $phone = $request->phone;
		  $check_valid_otp = DB::table('manage_otp')->where('phone', $phone)->where('otp', $request->otp)->first();
           $email = $request->email;
		   $check_user = User::where('email', $email)->first();
        $phone = $request->phone;
        $check_phone = User::where('phone', $phone)->first();
  
		  if(!empty($check_valid_otp) &&  empty($check_user) && empty($check_phone) ){
		  	DB::table('manage_otp')->where('phone', $request->phone)->delete();
            
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            $user->role = '2';
            $user->status = '1';

            $user->save();

		  	$this->return['message'] = 'Registration Successfully.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $request->all();
			$status = 200;

		  }else{

			$this->return['message'] = 'Invalid OTP';
			$this->return['status'] = 0;
			$status = 401;
		  }

		return response()->json($this->return, 200);  


  }


   public function login(Request $request) {

        $validator = Validator::make($request->all(), [
                    'phone' => 'required|digits:10|numeric', 'password' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

        

        if (Auth::attempt(['phone' => request('phone'), 'password' => request('password')])) {

            $user = Auth::user();

            if ($user->status == 1) {
                if ($user->role=='2') {
                         
                    $credentials = request(['phone', 'password']);

                    if (! $token = auth()->attempt($credentials)) {
                        return response()->json(['error' => 'Unauthorized'], 401);
                    }

                    $tokenData['user_id'] = $user->id;
                    $tokenData['device_token'] = 'device_tokken';
                    $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
                    $tokenData['exp'] = time() + (259200);
                    $jwtToken = $this->jwtobj->GenerateToken($tokenData);
                    $tokenData['access_token'] = $jwtToken;

                     $status = 200;
                     $this->return = [
                      'status' => true,
                      'message'=> 'Logged in successfully',
                      'result' => array_merge(
                        $user->toarray(), 
                        $tokenData
                      )
                    ];


                } else {
                    $this->return['message'] = 'Invalid Role.';
                    $this->return['status'] = 0;
                    $status = 401;
                }
            } else {
                $this->return['message'] = 'Your account is disabled. Please contact to Admin.';
                $this->return['status'] = 0;
                $status = 401;
            }
        } else {
            $this->return['status'] = 0;
            $this->return['message'] = 'Invalid credentials ';
            $status = 401;
        }

        return response()->json($this->return, 200);
    }




    
    // ApI method for logout
    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }


     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    // /**
    //  * Log the user out (Invalidate the token).
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function logout()
    // {
    //     auth()->logout();

    //     return response()->json(['message' => 'Successfully logged out']);
    // }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    // public function refresh()
    // {
    //     return $this->respondWithToken(auth('api')->refresh());
    // }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    // protected function respondWithToken($token)
    // {
    //     return response()->json([
    //         'access_token' => $token,
    //         'token_type' => 'bearer',
    //         'expires_in' => auth()->factory()->getTTL() * 60
    //     ]);
    // }

  
 

 public function forget_password(Request $request){

      $inputs = $request->all();
      if(isset($inputs['email']) && $inputs['email'] !=''){
          $check_user = User::where('email', $inputs['email'])->first();
          if(!empty($check_user)){
              $otp = rand(1000, 9999);
              $request->otp= $otp;

              DB::table('manage_otp')->where('email', $request->email)->delete();
              DB::table('manage_otp')->insert(array('otp'=>$otp, 'email'=>$request->email));

              $this->return['message'] = 'OTP Sent Successfully.';
              $this->return['status'] = 1;
              $this->return['jsonData'] = $request->all();
              $this->return['jsonData']['otp'] = $otp;
              $status = 200;
          }else{
              $this->return['message'] = 'Invalid email';
              $this->return['status'] = 0;
              $status = 401;
            
          }

      } else if(isset($inputs['phone']) && $inputs['phone'] !=''){
          $check_phone = User::where('phone', $inputs['phone'])->first();
          if(!empty($check_phone)){
              $otp = rand(1000, 9999);
              $request->otp= $otp;

              DB::table('manage_otp')->where('phone', $request->phone)->delete();
              DB::table('manage_otp')->insert(array('phone'=>$request->phone , 'otp'=>$otp));

              $this->return['message'] = 'OTP Sent Successfully.';
              $this->return['status'] = 1;
              $this->return['jsonData'] = $request->all();
              $this->return['jsonData']['otp'] = $otp;
              $status = 200;
          }else{
              $this->return['message'] = 'Invalid phone';
              $this->return['status'] = 0;
              $status = 401;
          }

      }else{
        $this->return['message'] = 'phone or email is required';
        $this->return['status'] = 0;
        $status = 401;
      }


      return response()->json($this->return, 200);



 }



   public function save_forget_password(Request $request){

    $validator = Validator::make($request->all(), [
       'otp' => 'required','password' => 'required',  'confirm_password' => 'required', 
  ]);

  if ($validator->fails()) {
        $this->return['status'] = 0;
        $this->return['message'] = $validator->errors()->first();
        $this->return['jsonData']['error'] = $validator->errors();
        $status = 200;
        return response()->json($this->return, $status);
    }


      $inputs = $request->all();

      if(isset($inputs['email']) && $inputs['email'] !='' && isset($inputs['otp']) && $inputs['otp']!='' ){


                  $email = $request->email;
                  $check_valid_otp = DB::table('manage_otp')->where('email', $email)->where('otp', $request->otp)->first();
                  $email = $request->email;
                  $check_user = User::where('email', $email)->first();

              if(!empty($check_valid_otp) &&  !empty($check_user)  ){

                   $password='';
                  if(isset($inputs['password']) && $inputs['password'] !='' && isset($inputs['confirm_password']) && $inputs['confirm_password'] !='' ){
                     $password =$inputs['password'];
                    if($inputs['password'] !=$inputs['confirm_password']){
                        $this->return['message'] = 'password and confirm_password should be same';
                        $this->return['status'] = 0;
                        $status = 401;
                        return response()->json($this->return, 200); die;

                    }

                  } 

                  DB::table('manage_otp')->where('email', $request->email)->delete();

                   $check_user->email = $request->email;
                   if($password !=''){
                     $user->password = Hash::make($request->password);
                     $check_user->save();
                   }
                                     

                    $tokenData['user_id'] = $check_user->id;
                    $tokenData['device_token'] = 'device_tokken';
                    $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
                    $tokenData['exp'] = time() + (259200);
                    $jwtToken = $this->jwtobj->GenerateToken($tokenData);
                    $tokenData['access_token'] = $jwtToken;

                     $status = 200;
                     $this->return = [
                      'status' => true,
                      'message'=> 'Password Changed Successfully.',
                      'result' => array_merge(
                        $check_user->toarray(), $tokenData
                      )
                    ];



              }else{

                $this->return['message'] = 'Invalid OTP';
                $this->return['status'] = 0;
                $status = 401;
              }
          

      } else if(isset($inputs['phone']) && $inputs['phone'] !='' && isset($inputs['otp']) && $inputs['otp']!='' ){

              $phone = $request->phone;
                  $check_valid_otp = DB::table('manage_otp')->where('phone', $phone)->where('otp', $request->otp)->first();
                  $phone = $request->phone;
                  $check_user = User::where('phone', $phone)->first();

                  //print_r($check_user);  die;

              if(!empty($check_valid_otp) && !empty($check_user)  ){

                   $checkpassword='';
                  if(isset($inputs['password']) && $inputs['password'] !='' && isset($inputs['confirm_password']) && $inputs['confirm_password'] !='' ){ 
                     $checkpassword =$inputs['password'];
                    if($inputs['password'] !=$inputs['confirm_password']){ 
                        $this->return['message'] = 'password and confirm_password should be same';
                        $this->return['status'] = 0;
                        $status = 401;
                        return response()->json($this->return, 200); die;

                    }

                  } 

                  DB::table('manage_otp')->where('phone', $request->phone)->delete();
                   if($checkpassword !=''){  
                     $password_val = Hash::make($request->password);
                     $check_user->password = $password_val;
                     $check_user->save();
                   }

                   
                    $tokenData['user_id'] = $check_user->id;
                    $tokenData['device_token'] = 'device_tokken';
                    $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
                    $tokenData['exp'] = time() + (259200);
                    $jwtToken = $this->jwtobj->GenerateToken($tokenData);
                    $tokenData['access_token'] = $jwtToken;

                     $status = 200;
                     $this->return = [
                      'status' => true,
                      'message'=> 'Password Changed Successfully.',
                      'result' => array_merge(
                        $check_user->toarray(), $tokenData
                      )
                    ];



              }else{

                $this->return['message'] = 'Invalid OTP';
                $this->return['status'] = 0;
                $status = 401;
              }
          

      }else{
        $this->return['message'] = 'phone or email and otp are required';
        $this->return['status'] = 0;
        $status = 401;
      }

    
     return response()->json($this->return, 200);


   }




}
