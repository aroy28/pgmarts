<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\OrderStatusDetails;
use App\Users_address;

class OrderController extends Controller
{
    //

     public function __construct()
	{
 //    	$this->middleware('auth');
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}


	public function my_orders(Request $request){

			$validator = Validator::make($request->all(), [
			 'user_id' => 'required',
			]);

			if ($validator->fails()) {
			    $this->return['status'] = 0;
			    $this->return['message'] = $validator->errors()->first();
			    $this->return['jsonData']['error'] = $validator->errors();
			    $status = 200;
			    return response()->json($this->return, $status);
			}

			$user_id = $request->user_id;
			$order = Orders::select('orders.*', 'status.name as order_status' )->join('status', 'status.id','orders.status')->where('user_id', $user_id);

			 if(!empty($request->status)){
	              $order = $order->where('status', $request->status);
	          }

	          $order = $order->orderby("id","DESC");

	          $orders_count = $order->get()->count();

	          $request->total = $request->total ? $request->total : 10;
	          $request->page = $request->page ? $request->page:1;

	           if($request->page==1){
	              $order = $order->offset(0)->limit($request->total);
	            }else if($request->page > 1){
	              $offset = ($request->page - 1)*$request->total ;
	              $order = $order->offset($offset)->limit($request->total);
	            }else{
	              $order = $order->offset(0)->limit($request->total);
	            }


				$order_summary = $order->get()->toArray();
				$order_summarys =[];
				foreach($order_summary as $dat){
					$addresid = $dat['address_id'];
                    $addresdat= Users_address::getAddressbyid($addresid);
                    $dat['address'] = $addresdat;
                    $order_summarys[] = $dat;
				}

				$records = array('my_order'=>$order_summarys, 'page'=>$request->page , 'per_page_total'=>$request->total , 'total_records'=>$orders_count );

				$this->return['message'] = 'Order list.';
				$this->return['status'] = 1;
				$this->return['jsonData'] = $records;
				$status = 200;

              return response()->json($this->return, 200);



	}

 // save or update order status in order table and track order status record
	public function save_order_status(Request $request) {
			$validator = Validator::make($request->all(), [
			 'user_id' => 'required','order_id' => 'required','order_status' => 'required|Integer'
			]);

			if ($validator->fails()) {
			    $this->return['status'] = 0;
			    $this->return['message'] = $validator->errors()->first();
			    $this->return['jsonData']['error'] = $validator->errors();
			    $status = 200;
			    return response()->json($this->return, $status);
			}

            $orders_data = Orders::where('id', $request->order_id)->where('user_id', $request->user_id)->first();
			if($orders_data){
				$data =[
					'order_id'=>$request->order_id,
					'user_id'=>$request->user_id,
					'status_id'=>$request->order_status
	            ];
				OrderStatusDetails::saveOrderStatus($data);
				$orders_data->status = $request->order_status;
				$orders_data->save();
                
                $orderId = $request->order_id;
				send_updated_order_status_email($orderId);
				//send_order_status_sms($orderId);
				$this->return['message'] = 'Order updated sucessfully';
				$this->return['status'] = 1;
				$this->return['jsonData'] = $data;
			}else{
				$this->return['message'] = 'Order not found';
				$this->return['status'] = 0;
			}
			return response()->json($this->return, 200);
	}


	public function order_track_status_history(Request $request){
           $validator = Validator::make($request->all(), [
			 'user_id' => 'required','order_id' => 'required'
			]);

           if ($validator->fails()) {
			    $this->return['status'] = 0;
			    $this->return['message'] = $validator->errors()->first();
			    $this->return['jsonData']['error'] = $validator->errors();
			    $status = 200;
			    return response()->json($this->return, $status);
			}
            $order_id = $request->order_id;
            $user_id = $request->user_id;
			$orderStatus = OrderStatusDetails::select('order_status_details.*', 'status.name as order_status' )->join('status', 'status.id','order_status_details.status_id')->where('order_status_details.user_id', $user_id)->where('order_status_details.order_id', $order_id)->orderby('order_status_details.id', 'ASC')->get();

			$this->return['message'] = 'Order Status list.';
			$this->return['status'] = 1;
			$this->return['jsonData'] = $orderStatus;
			return response()->json($this->return, 200);

	}



}
