<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

   // login post function and check credential here
    public function postLogin(Request $request) {

        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

         if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) )
          {     
            if(Auth::user()->role=='2'){
               Auth::logout();
               Session::flush(); 
               return redirect()->intended('/login')->withInput($request->input())->with('loginmsg', "Login failed!");
             }

            if(Auth::user()->role=='1'){
               return redirect()->intended('/home');
             }

             if(Auth::user()->role=='3'){
               return redirect()->intended('/deliver_boy/get_orders');
             } else {
               return redirect()->intended('/home');
             }
              
          }
          
         return redirect()->intended('/login')->withInput($request->input())->with('loginmsg', "Login failed!");


    }
}
