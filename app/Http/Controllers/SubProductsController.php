<?php
namespace App\Http\Controllers;
use App\Sub_Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Main_Products;
class SubProductsController extends Controller
{
public function __construct()
{
$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$products = Sub_Products::paginate(1);
$data['active_class']='subproduct';
$data['main_product']=Main_Products::All();
return view('sub_product.list',$data)->with('products',$products);
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
$data['products_data']='create';
$data['active_class']='subproduct';
return view('sub_product.create',$data);
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//dd($request->all());
$this->validate($request, [
'product_name' => 'required|min:3|max:50',
'product_description' => 'required|min:10',
'product_image'=>'image'
]);
//ADD PHOTO
if (isset($request->product_image)){
$avatar= Input::file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if($size[0] > 1280 && $size[1] > 720) {//RESIZE PHOTO
Image::make($avatar)->resize(1280, 720)->save(public_path('/photo/' . $filename));
}
else{
Image::make($avatar)->save(public_path('/photo/' . $filename));
}
}
//ADD PRODUCT_NAME, PRODUCT_DESCRIPTION, AMOUNT AND QUANTITY
$product = new Sub_Products;
$product->product_name = $request->input('product_name');
$product->product_description = $request->input('product_description');
$product->main_products_id = $request->input('main_products_id');
if (isset($request->product_image)){
$product->product_image = $filename;
}
$product->save();
return redirect('/subproducts')->with('success');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
$data['products'] = Sub_Products::where('id',$id)->first();
$main_product=Main_Products::where('id',$data['products']->main_products_id)->first();
if (!empty($main_product)) {
$data['mainproduct']=$main_product->product_name;
# code...
}
else
{
$data['mainproduct']='--';
}
return view('sub_product.show',$data);
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit(Products  $product)
{
return view('sub_product.edit')->with('product',$product);
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
// dd($request->all());
$products = Sub_Products::find($id);
$products->product_name = $request->product_name;
$products->product_description = $request->product_description;
$products->main_products_id = $request->main_products_id;
//resize photo
if (isset($request->product_image)){
# code...
if($request->hasFile('product_image')){
$avatar = $request->file('product_image');
$filename = time() . '.' . $avatar->getClientOriginalExtension();
$size = getimagesize($avatar);
if ($size[0]>1280 && $size[1]>720){
Image::make($avatar)->resize(1280,720)->save(public_path('photo/').$filename);
}
else{
Image::make($avatar)->save(public_path('photo/').$filename);
}
$oldFilename = $products->product_image;
//update the database
$products->product_image=$filename;
//Delete the old photo
Storage::delete($oldFilename);
}
}
$products->save();
return redirect('/subproducts') -> with('edit','');
}
/**
* Remove the specified resource from storage.
*
* @param  Products $product
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$products = Sub_Products::where('id',$id)->take(1)->delete();
return redirect('/subproducts') -> with('delete',' ');
}
//FRONTEND CONTROL -  ANY PRODUCT SHOW LIST, AND  INDIVIDUAL PRODUCT VIEW
public function product_show(){
$products = Sub_Products::orderby('id','desc')->paginate(5);
return view('sub_product.product_show')->with('products',$products);
}
public function product_view(Products $product){
return view('sub_product.product_view')->with('products',$product);
}
public function products_edit($id)
{
$data['product']=Sub_Products::where('id',$id)->first();
$data['products_data']='edit';
$data['active_class']='subproduct';
return view('sub_product.create',$data);
}
public function getsubproduct($id)
{
$data['products']=Sub_Products::where('main_products_id',$id)->orderby('id','desc')->get();
return view('sub_product.getsubproduct',$data);
}
}