<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Cart;

class MycartController extends Controller
{
    //
     public function __construct()
	{
 //    	$this->middleware('auth');
	 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
	}

	public function add_to_cart(Request $request){

		$validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'product' => 'required',  
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

        if(isset($request->product) &&!empty($request->product)){
                 foreach($request->product as $dat){
                     if($dat['id'] =='' || $dat['product_price'] =='' || !isset($dat['quantity']) || $dat['quantity'] ==''){
                        $this->return['message'] = 'Please check product id,product price, quantity in Products data';
                        $this->return['status'] = 0;
                        $this->return['jsonData']['data'] = $request->all();
                        return response()->json($this->return, 200);  die;
                     }

                 }
        }
        
        $user_id=$request->user_id; 
        $products = $request->product;

        foreach($products as $val){
                $cart_data = Cart::where('product_id', $val['id'])->where('user_id', $user_id)->first();
                if(!empty($cart_data)){
                    $cart_data->no_of_product = $val['quantity'];
                    $cart_data->save();
                }else{
                    $created_at = Date('Y-m-d h:i:s'); 
                    $cart = new Cart;
                    $cart->user_id = $request['user_id'];
                    $cart->product_id = $val['id'];
                    $cart->no_of_product = $val['quantity'];
                    $cart->product_price = $val['product_price'];
                    $cart->product_name = $val['name'];
                    $cart->save();
                }
        }
        

		$this->return['message'] = 'Product added in cart Successfully.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;

       return response()->json($this->return, 200);




    }


   //remove data from cart
    public function remove_from_cart(Request $request){
       
       $validator = Validator::make($request->all(), [
		     'user_id' => 'required', 'product_id' => 'required',  
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }
        
        $user_id=$request->user_id; 
        $product_id = $request->product_id;

        $arr = explode(',', $product_id);  
        $cart_data = Cart::whereIn('product_id', $arr)->where('user_id', $user_id)->delete();
		$this->return['message'] = 'Product removed from cart Successfully.';
		$this->return['status'] = 1;
		$this->return['jsonData'] = $request->all();
		$status = 200;

       return response()->json($this->return, 200);




    }


    //get all cart data 
    public function get_cart_data(Request $request){
        $validator = Validator::make($request->all(), [
		     'user_id' => 'required',
		]);

		if ($validator->fails()) {
            $this->return['status'] = 0;
            $this->return['message'] = $validator->errors()->first();
            $this->return['jsonData']['error'] = $validator->errors();
            $status = 200;
            return response()->json($this->return, $status);
        }

		$user_id=$request->user_id; 
		$cart_data=	DB::table('cart')
			->join('products', 'cart.product_id', '=', 'products.id')
			->select('cart.id as cartid','cart.no_of_product', 'products.*')
			->where('user_id', $user_id)
			->get();

       $total = count($cart_data);

		$this->return['message'] = 'Cart list.';
		$this->return['status'] = 1;
		$this->return['jsonData']['total'] = $total;
		$this->return['jsonData']['my_cart'] = $cart_data;
		
		$status = 200;

		return response()->json($this->return, 200);
	   		



    }






}
