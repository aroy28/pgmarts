<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userwallets;

use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


class UserwalletController extends Controller
{
	    //
	     public function __construct()
		{
	 //    	$this->middleware('auth');
		 $this->return = ['status' => 1, 'message' => 'success', 'jsonData' =>array()];
		}

		
		public function get_my_wallet(Request $request){	

				$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				]);

				if ($validator->fails()) {
				$this->return['status'] = 0;
				$this->return['message'] = $validator->errors()->first();
				$this->return['jsonData']['error'] = $validator->errors();
				$status = 200;
				return response()->json($this->return, $status);
				} 

				$user_id = $request->user_id;

				$Userwallets = Userwallets::where('user_id', $user_id);
	            $Userwallets = $Userwallets->get()->toArray();
	            
	            $sum=0;
	            foreach($Userwallets as $walet){
	             $sum= $sum + $walet['amount'];

	            }
                
                $wallet = array('wallet_amount'=>$sum);

				$this->return['message'] = 'wallet data.';
				$this->return['status'] = 1;
				$this->return['jsonData'] = $wallet;
				$status = 200;

				return response()->json($this->return, 200);	

		}


		public function add_my_wallet(Request $request){
			$validator = Validator::make($request->all(), [
			'user_id' => 'required','amount' => 'required|integer',
			]);

			if ($validator->fails()) {
			 $this->return['status'] = 0;
			 $this->return['message'] = $validator->errors()->first();
			 $this->return['jsonData']['error'] = $validator->errors();
			 $status = 200;
			 return response()->json($this->return, $status);
			} 

			$wallet = new Userwallets;
			$wallet->user_id = $request->user_id;
			$wallet->amount = $request->amount;
			$wallet->description = "";
			$wallet->save();
            
			$this->return['message'] = 'Amount updated in wallet.';
			$this->return['status'] = 1;
			$status = 200;

			return response()->json($this->return, 200);


		}



}
