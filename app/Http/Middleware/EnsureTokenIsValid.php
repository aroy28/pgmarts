<?php

namespace App\Http\Middleware;

use Closure;
use App\CreatorJwt;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  
  public $jwtobj;

    public function __construct() {
      $this->jwtobj = new CreatorJwt();
    }


    public function handle($request, Closure $next)
    {    
        $req_headers = getallheaders();
         if(isset($req_headers['Authorization']) && !empty($req_headers['Authorization']) ){

                $jwtData = $this->jwtobj->DecodeToken($req_headers['Authorization']);
                //print_r($jwtData); die();
                if (array_key_exists("expired_token",$jwtData) || array_key_exists("token_invalid",$jwtData) )
                {
                     $return['message'] = 'Invalid token';
                     $return['status'] = 0;
                    return response()->json($return, 401); die;

                }

                // else{
                //     if($jwtData['user_id'] !='1'){  
                //           if($jwtData['user_id'] !=$req_headers['user_id']){
                //            http_response_code('401');
                //            echo json_encode(array( 'status' => false, 'message' =>'' ));
                //             die;
                //           }
                //      }
                     
                // }

         }else{
            $return['message'] = 'Invalid token';
            $return['status'] = 0;
            return response()->json($return, 401); die;

         }
        // if ($request->input('token') !== 'my-secret-token') {
        //     //return redirect('home');
        //     $return['message'] = 'Invalid token';
        //     $return['status'] = 0;
        //      return response()->json($return, 401); die;
        // }


        return $next($request);
    }
}
