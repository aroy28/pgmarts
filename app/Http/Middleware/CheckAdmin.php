<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    
        // if(Auth::user()->role!='1'){
        //     Auth::logout();
        //     Session::flush(); 
        //     return redirect()->intended('/login')->withInput($request->input())->with('loginmsg', "Access failed!");
        //  }

        return $next($request);
    }
}
