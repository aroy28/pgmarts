<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_address extends Model
{
    //

	protected $table = 'users_address';
	public $timestamps = false;

	public static function getAddressbyid($addresid){
		return Users_address::where('id', $addresid)->get();
	}


}
