<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusDetails extends Model
{
    protected $table = 'order_status_details';
    public $timestamps = false;


    public static function saveOrderStatus($data){
    	 OrderStatusDetails::insert($data);
    }


}
