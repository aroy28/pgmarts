<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@product_show');

Route::get('/show/{product}','ProductsController@product_view');

Route::resource('/products','ProductsController');



Route::get('products/{product}', 'ProductsController@show');

Route::get('products_edit/{id}', 'ProductsController@products_edit');

Route::get('products_edit/{id}', 'ProductsController@products_edit');
Route::resource('/mainproducts','MainProductsController');
Route::get('main_products/{id}', 'MainProductsController@products_edit');
Route::get('main_products_delete/{id}', 'MainProductsController@destroy');

Route::resource('/subproducts','SubProductsController');
Route::get('sub_products/{id}', 'SubProductsController@products_edit');
Route::get('sub_products_delete/{id}', 'SubProductsController@destroy');
Route::get('products/fetch_subproduct/{id}', 'ProductsController@fetch_subproduct');
Route::get('products_edit/fetch_subproduct/{id}', 'ProductsController@fetch_subproduct');
Route::get('getsubproduct_by_mainproduct/{id}', 'SubProductsController@getsubproduct');
Route::get('getsubproduct_id_by_mainproduct/{id}', 'ProductsController@fetch_subproduct');
Route::get('getproduct_by_subproduct/{id}', 'ProductsController@getproduct');
Route::get('subproducts_details/{id}', 'SubProductsController@show');
Route::get('prod_del/{id}', 'ProductsController@destroy');
// Route::get('photos/create/{id}','PhotoController@create');


Auth::routes();
Route::post('login', 'Auth\LoginController@postLogin');

Route::get('logout', function(){
	if(Auth::check())
	    Auth::logout();
	Session::flush();
    return redirect('/');
})->name('logout');

Route::get('/home', 'ProductsController@index');

//DeliverBoy
Route::get('deliver_boy/get_orders', 'DeliverBoyController@getOrders')->name('deliver-boy-orders');
Route::get('get_order_details/{id}', 'DeliverBoyController@getOrderDetails')->name('get_order_details');
Route::post('change_order_status', 'DeliverBoyController@change_order_status')->name('change_order_status');

Route::group(['middleware' => ['checkadmin']], function() {
Route::get('add_user', 'UserController@addUser')->name('add_user');
Route::post('add_user', 'UserController@postAddUser')->name('postaddUser');
Route::get('user_list', 'UserController@user_list')->name('user_list');
Route::get('delete_user/{id}', 'UserController@delete_user')->name('delete_user');
Route::get('restore_user/{id}', 'UserController@restore_user')->name('restore_user');
Route::get('edit_user/{id}', 'UserController@edit_user')->name('edit_user');
Route::post('update_user', 'UserController@update_user')->name('update_user');
});
// Route::resource('/album', 'AlbumController');




