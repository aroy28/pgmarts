<?php

use Illuminate\Http\Request;
use App\Http\Middleware\EnsureTokenIsValid;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});






Route::group(['middleware' => ['cors']], function() {

	
	Route::post('send_otp', 'LoginController@send_otp');
	Route::post('resend_otp', 'LoginController@resend_otp');
	Route::post('register', 'LoginController@register');
	Route::post('login', 'LoginController@login');
	Route::post('forget_password', 'LoginController@forget_password');
	Route::post('save_forget_password', 'LoginController@save_forget_password');
    
    Route::post('get_order_status_list', 'CheckoutController@get_order_status_list'); 
	Route::post('main_products', 'ProductlistController@main_products');
	Route::post('sub_products', 'ProductlistController@sub_products');
	Route::post('products', 'ProductlistController@products');
	Route::post('products_list', 'ProductlistController@products_list');
	Route::post('delivery_charges', 'ProductlistController@delivery_charges');
	Route::post('units', 'ProductlistController@get_units');
   
   Route::middleware([EnsureTokenIsValid::class])->group(function () {
       
       	Route::post('my_orders', 'OrderController@my_orders');
		Route::post('save_order_status', 'OrderController@save_order_status');
		Route::post('order_track_status_history', 'OrderController@order_track_status_history');
		Route::post('get_my_address', 'AddressController@get_my_address');
       	Route::post('delete_my_address', 'AddressController@delete_my_address');
		Route::post('add_address', 'AddressController@add_address');
		Route::post('edit_address', 'AddressController@edit_address');
		Route::post('get_address_by_id', 'AddressController@get_address_by_id');
		Route::post('get_my_wallet', 'UserwalletController@get_my_wallet');
		Route::post('add_my_wallet', 'UserwalletController@add_my_wallet');
		Route::post('add_post', 'PostController@add_post');
		Route::post('update_password', 'AddressController@update_password');
		Route::post('add_to_cart', 'MycartController@add_to_cart');
		Route::post('remove_from_cart', 'MycartController@remove_from_cart');
		Route::post('get_cart_data', 'MycartController@get_cart_data');

		Route::post('apply_coupon', 'CouponController@apply_coupon');
		Route::post('initialize_payment_checkout', 'CheckoutController@initialize_payment_checkout');
		Route::post('final_payment_checkout', 'CheckoutController@final_payment_checkout');
		Route::post('update_checkout_order_status', 'CheckoutController@update_checkout_order_status');

	    Route::post('logout', 'LoginController@logout');

    });


});



